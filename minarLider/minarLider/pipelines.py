# -*- coding: 850 -*-# -*- coding: 850 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import csv
from minarLider.items import MinarliderItem

class MinarliderPipeline(object):

        def __init__(self):
            self.csv_filename = 'blank'
            self.fieldnames = ['Descrip', 'Precio', 'Unidades']

        def open_spider(self, spider):
            self.csv_filename = 'Lider.csv'


        def close_spider(self, spider):
            pass

        def process_item(self, item, spider):
            xitem = MinarliderItem()
            x = len(item['Precio'])
            if x<len(item['Descrip']):
                x=len(item['Descrip'])
            if x<len(item['Unidades']):
                x=len(item['Unidades'])
            with open(self.csv_filename, 'a',  newline='\n') as outfile:
                spamwriter = csv.DictWriter(outfile, fieldnames=self.fieldnames, lineterminator='\n',dialect='excel')
                spamwriter.writeheader()
                i = 0
                while i < x:
                    xitem['Descrip'] = item['Descrip'][i]
                    xitem['Precio'] = item['Precio'][i]
                    xitem['Unidades'] = item['Unidades'][i]
                    spamwriter.writerow(xitem)
                    i= i + 1
