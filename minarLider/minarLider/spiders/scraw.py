# -*- coding: utf-8 -*-
from typing import Any, Union

import scrapy
#from scrapy import Request
from scrapy.http import response
from scrapy.http import HtmlResponse
from idna import unicode
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose
from scrapy.selector import Selector

from minarLider.items import MinarliderItem

class ScrawSpider(scrapy.Spider):
    name = 'scraw'
    allowed_domains = ['web']
    #              https://www.lider.cl/supermercado/category/Frescos-L%C3%A1cteos/Frutas-y-verduras/Verduras/_/N-irzgu5?No=0&isNavRequest=Yes&Nrpp=40&page=1
    start_urls = ['https://www.lider.cl/supermercado/category/Frescos-L%C3%A1cteos/Frutas-y-verduras/Verduras/_/N-irzgu5?No=40&isNavRequest=Yes&Nrpp=40&page=2']

    def parse(self, response):
        urls = response.xpath('//ul[@class="pagination pull-right"]/li/a/@href').extract()
        self.file = open('items.txt', 'w')
        #urls = response.xpath('//span[@class="product-description js-ellipsis"]/text()').extract()
        # log.msg('Entrando de la Funcio Parse')
        for curl in urls:
            curl = 'https://www.lider.cl'+curl
            # self.logger.info('A response from %s just arrived!', curl)
            yield scrapy.Request(curl, callback=self.parse_pages,dont_filter=True)

            # break

        # self.file.close()

    def parse_pages(self,response):
         # item = MinarliderItem()

         self.d = response.xpath('//span[@class="product-description js-ellipsis"]/text()').extract()
         self.p = response.xpath('//div[@class="product-price"]/span[@class="price-sell"]/b/text()').extract()
         self.u = response.xpath('//div[@class="product-price"]/span[@class="product-attribute"]/text()').extract()
         tam = len(self.d)
         # line = '{0}\n'.format(tam)
        # # self.file.write(line)
         cont = 0
         while cont < tam:
             line = self.d[cont]+','+self.p[cont]+','+self.u[cont]+'\n'
             self.file.write(line)
             cont = cont + 1
        # log.msg('Termino la Funcio Parse')

         # return item

         l = ItemLoader(item=MinarliderItem(), response=response)

         l.add_xpath('Descrip', '//span[@class="product-description js-ellipsis"]/text()', MapCompose(unicode.strip, unicode.title))
         l.add_xpath('Precio','//div[@class="product-price"]/span[@class="price-sell"]/text()' , MapCompose(unicode.strip, unicode.title))
         l.add_xpath('Unidades','//div[@class="product-price"]/span[@class="product-attribute"]/text()' , MapCompose(unicode.strip, unicode.title))
         return l.load_item()
        #self.log(l.load_item())
        # pass



        # product_grid = response.xpath('//ul[@class="product ListergridView"]')
        # if product_grid:
        #     for product in self.handle_product_listings(response):
        #         yield product
        # pages = response.xpath('//ul[@class="categories shelf"]/li/a')
        # if not pages:
        #    pages = response.xpath('//ul[@class="categories aisles"]/li/a')
        # if not pages:
        #   # here is something fishy
        #    return
        #
        # for url in pages:
        #     yield response.follow(url, callback=self.parse_pages,dont_filter=True)
        # pass


# Para la Extraccion de Información que necesitamos
#grid = response.xpath('//span[@class="product-description js-ellipsis"]').extract()  ======>  Nombre del producto
#grid = response.xpath('//div[@class="product-price"]/span[@class="product-attribute"]').extract()  ===> Unidades
#grid = response.xpath('//div[@class="product-price"]/span[@class="price-sell"]').extract()    ===>Precio
