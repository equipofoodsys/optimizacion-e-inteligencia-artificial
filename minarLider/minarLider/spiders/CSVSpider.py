import scrapy
#from scrapy import log
from scrapy import log

from minarLider.items import MinarliderItem
from scrapy.spiders import CSVFeedSpider

class CSVSpiderSpider(CSVFeedSpider):
       name = 'lider'
       allowed_domains = ['web']
       #              https://www.lider.cl/supermercado/category/Frescos-L%C3%A1cteos/Frutas-y-verduras/Verduras/_/N-irzgu5?No=0&isNavRequest=Yes&Nrpp=40&page=1
       start_urls = [
           'https://www.lider.cl/supermercado/category/Frescos-L%C3%A1cteos/Frutas-y-verduras/Verduras/_/N-irzgu5?No=40&isNavRequest=Yes&Nrpp=40&page=2']
       delimiter = ';'
       quotechar = "'"
       headers = ['Descrip', 'Precio', 'Unidades']

       def parse_row(self, response, row):
           urls = response.xpath('//ul[@class="pagination pull-right"]/li/a/@href').extract()
           # urls = response.xpath('//span[@class="product-description js-ellipsis"]/text()').extract()
           for curl in urls:
               curl = 'https://www.lider.cl' + curl
               # self.logger.info('A response from %s just arrived!', curl)
               yield scrapy.Request(curl, callback=self.parse_pages, dont_filter=True)
               break

       def parse_pages(self, response, row):
               item = MinarliderItem()
               log.msg('Hi, this is a row!: %r' % row)
               # item['Descrip'] = row['Descrip']
               # item['Precio'] = row['Precio']
               # item['Unidades'] = row['Unidades']
               return item
