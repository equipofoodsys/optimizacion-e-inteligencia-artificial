
from scrapy.exporters import BaseItemExporter
import io
import csv

from minarLider import settings


class CsvItemExporter(BaseItemExporter):
    fieldnames_standard = ['Descrip', 'Precio', 'Unidades']

    def __init__(self, file, **kwargs):
        self._configure(kwargs)
        kwargs['fields_to_export'] = settings.getlist('EXPORT_FIELDS')
        if not self.encoding:
           self.encoding = 'utf-8'
        self.file = io.TextIOWrapper(file,
                                 line_buffering=False,
                                 write_through=True,
                                 encoding=self.encoding)
        self.file.truncate(0)
        self.items = []

    def finish_exporting(self):
        spamwriter = csv.DictWriter(self.file,
                           fieldnames=self.__get_fieldnames(),
                           lineterminator='\n')
        spamwriter.writeheader()
        for item in self.items:
           spamwriter.writerow(item)

    def export_item(self, item):
        pass

    def __get_fieldnames(self):
        field_names = set()

        for product in self.items:
          for key in product.keys():
            if key not in self.fieldnames_standard:
              field_names.add(key)
        return self.fieldnames_standard + list(field_names)
