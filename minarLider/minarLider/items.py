# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class MinarliderItem(scrapy.Item):
    Unidades = scrapy.Field()
    Precio = scrapy.Field()
    Descrip = scrapy.Field()
    pass

