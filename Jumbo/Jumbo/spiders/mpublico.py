import requests
import scrapy


from Jumbo.items import JumboItem
import json

class mublicoSpiderSpider(scrapy.Spider):
    name = 'publico'
    allowed_domains = ['mercadopublico.cl']

    def __init__(self, category=None, *args, **kwargs):
        self.start_urls = ['http://www.mercadopublico.cl/Home/Contenidos/NuevaTiendaFrame']
        # self.url='http://www.mercadopublico.cl/TiendaFicha/Ficha/ObtenerProveedores'
        #self.url = 'http://www.mercadopublico.cl/TiendaBuscador/Buscar/Index'
        self.url = 'http://www.mercadopublico.cl/TiendaBuscador'

    def parse(self, response):
        # xdata = { 'hdMenorPrecioProducto' : "$ 250", 'hdCondDespachoDias':''}
        xdata = {'q': '1468748'} # Para extraer Lista de precios de un producto!
        # FormRequest ( url = "http://www.example.com/post/action" ,formdata = { 'name' : 'John Doe' , 'age' : '27' },callback = self . after_post )
        yield scrapy.FormRequest(url=self.url, formdata=xdata, callback=self.parse_pages)

    def parse_pages(self, response):
        self.log("**** ***** *** Url Extraida: %s====>>>" % response.url)
        self.log("**** ***** *** Data Extraida: ====>>>   %s" % response.body )
        self.log("**** ***** *** Url Extraida: %s====>>>" % response.url)