# -*- coding: utf-8 -*-
import scrapy
from Jumbo.items import JumboItem

class JumbospiderSpider(scrapy.Spider):
    name = 'JumboSpider'
    allowed_domains = ['web']

    def __init__(self, category=None, *args, **kwargs):
        self.Urls = 'https://nuevo.jumbo.cl/buscapagina?sl=3a356ef2-a2d4-4f1b-865f-c79b6fcf0f2a&PS=18&cc=18&sm=0&PageNumber={}&&fq=C:/20/21/'
        self.start_urls = [self.Urls.format(1)]  # ,Urls.format(2)]
        # self.va=False

    def parse(self, response):
        rango=[1,2,3,4,5,6,7,8,9,10]
        va=[False,False]
        for x in rango:
            curl = self.Urls.format(x)
            yield scrapy.Request(curl, callback=self.check_pages(response,va), dont_filter=False)
            if va[0]:
                try:
                  yield scrapy.Request(curl, callback=self.parse_pages, errback=self.handle_error,dont_filter=True)
                except:
                    pass
            else:
                break

    def check_pages(self, response,va):
        va[0]=len(response.xpath('//*[@class="product-item__name"]').extract_first())>0

    # def parse_pages(self, response):
    #     self.log("Nombre del Producto: %s" % response.xpath('//*[@class="product-item__name"]/text()').extract())
    #     self.log("Precio del Producto: %s" % response.xpath('//*[@class="product-prices__value product-prices__value--best-price"]/text()').extract())
    #     pass

    def parse_pages(self, response):
        item = JumboItem()
        item['Descrip'] = response.xpath('//*[@class="product-item__name"]/text()').extract()
        item['Precio'] = response.xpath('//*[@class="product-prices__value product-prices__value--best-price"]/text()').extract()
        item['Unidades'] = response.xpath('//*[@class="product-prices__value product-prices__value--best-price"]/text()').extract()
        return item

    def handle_error(self, failure):
        url = failure.request.url
        self.log.error('Failure type: %s, URL: %s', failure.type, url)


    # self.log("Precio del Producto X Unidades: %s" % response.xpath(
#     '//*[@class="product-prices__price product-prices__price--regular-price"]').extract())  # Tratemos de obtener el precio
#product-prices__value product-prices__value--ppum  /*/[@class="product-prices__value-wrapper"]/span/text()
    # Urls='https://nuevo.jumbo.cl/frutas-y-verduras/verduras?PS={}'

