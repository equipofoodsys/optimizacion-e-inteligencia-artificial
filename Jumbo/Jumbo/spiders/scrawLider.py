import scrapy
from Jumbo.items import JumboItem

class ScrawSpider(scrapy.Spider):
    name = 'crawlider'
    allowed_domains = ['web']
    #              https://www.lider.cl/supermercado/category/Frescos-L%C3%A1cteos/Frutas-y-verduras/Verduras/_/N-irzgu5?No=0&isNavRequest=Yes&Nrpp=40&page=1
    start_urls = ['https://www.lider.cl/supermercado/category/Frescos-L%C3%A1cteos/Frutas-y-verduras/Verduras/_/N-irzgu5?No=40&isNavRequest=Yes&Nrpp=40&page=2']

    def parse(self, response):
        urls = response.xpath('//ul[@class="pagination pull-right"]/li/a/@href').extract()
        for curl in urls:
               si=False
               for ca in curl:
                   if ca=='/':
                    si=True
                    break
               if si:
                   curl = 'https://www.lider.cl' + curl
                   try:
                      yield scrapy.Request(curl, callback=self.parse_pages,dont_filter=True)
                   except:
                      pass

    def parse_pages(self,response):
          item = JumboItem()
          item['Descrip'] = response.xpath('//span[@class="product-description js-ellipsis"]/text()').extract()
          item['Precio'] = response.xpath('//div[@class="product-price"]/span[@class="price-sell"]/b/text()').extract()
          item['Unidades'] = response.xpath('//div[@class="product-price"]/span[@class="product-attribute"]/text()').extract()
          return item
