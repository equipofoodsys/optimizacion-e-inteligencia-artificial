# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from Jumbo.items import JumboItem
import csv

class JumboPipeline(object):

    def __init__(self):
        self.csv_filename = 'blank'
        self.fieldnames = ['Descrip', 'Precio', 'Unidades']

    def open_spider(self, spider):
        self.csv_filename = 'Jumbo.csv'


    def process_item(self, item, spider):
        xitem = JumboItem()
        x = len(item['Precio'])
        if x < len(item['Descrip']):
            x = len(item['Descrip'])
        # if x < len(item['Unidades']):
        #     x = len(item['Unidades'])
        with open(self.csv_filename, 'a', newline='\n') as outfile:
            spamwriter = csv.DictWriter(outfile, fieldnames=self.fieldnames, lineterminator='\n', dialect='excel')
            spamwriter.writeheader()
            i = 0
            while i < x:
                xitem['Descrip'] = item['Descrip'][i]
                xitem['Precio'] = item['Precio'][i]
                xitem['Unidades'] = item['Unidades'][i]
                spamwriter.writerow(xitem)
                i = i + 1
