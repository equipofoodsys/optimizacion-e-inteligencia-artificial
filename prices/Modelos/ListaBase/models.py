# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from Modelos.Producto.models import *

# Create your models here.
class ListaBase(models.Model):
    DescripcionSC = models.CharField(max_length=100)
    UnidadesSC = models.CharField(max_length=100)
    DescripcionC = models.CharField(max_length=100)
    UnidadesC = models.CharField(max_length=100)
    estado =  models.BooleanField(default = True)
    # productopk = models.ForeignKey(Producto, null= True, blank= True, on_delete = models.CASCADE)
        # models.ForeignKey(Producto, on_delete=models.SET_NULL, null=True)
    # actualizado = models.BooleanField(default = True)
