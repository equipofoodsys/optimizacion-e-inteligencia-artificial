from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.
class Region(models.Model):
    CodigoRegion = models.CharField( max_length=20)
    DescripcionRegion = models.CharField(unique=True, max_length=100)

    # def get_absolute_url(self):
    #     """
    #     Devuelve el URL a una instancia particular de Provee
    #     """
    #     return reverse('food:proveedor-detail', args=[str(self.id)])