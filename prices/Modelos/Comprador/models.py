from django.db import models
from django.core.urlresolvers import reverse
from  Modelos.Region.models import *
# Create your models here.

class Comprador(models.Model):
    CodigoOrganismoComprador = models.CharField(max_length=80)
    NombreOrganismoComprador = models.CharField(blank=True,max_length=200)
    RutUnidadComprador = models.CharField(blank=True,max_length=20)
    # CodigoUnidadComprador = scrapy.Field()
    # NombreUnidadComprador = scrapy.Field()
    ActividadComprador = models.CharField(blank=True,max_length=200)
    DireccionUnidadComprador = models.CharField(blank=True,max_length=100)
    # ComunaUnidadComprador = scrapy.Field()
    regionPk = models.ForeignKey(Region, null= True, blank= True, on_delete = models.CASCADE)
    #RegionUnidadComprador = models.CharField(blank=True,max_length=100)
    # PaisComprador = scrapy.Field()
    NombreContactoComprador = models.CharField(blank=True,max_length=80)
    CargoContactoComprador = models.CharField(blank=True,max_length=55)
    FonoContactoComprador = models.CharField(blank=True,max_length=80)
    MailContactoComprador = models.CharField(blank=True,max_length=50)

    def __str__(self):
        """
        Cadena que representa a la instancia particular del modelo
        """
        return self.CodigoOrganismoComprador + ' , ' + self.NombreOrganismoComprador + ': ' +self.RutUnidadComprador

    def get_absolute_url(self):
        """
        Devuelve el URL a una instancia particular de Provee
        """
        return reverse('food:comprador-detail', args=[str(self.id)])




