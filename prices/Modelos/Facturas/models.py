from django.db import models
from datetime import datetime
from Modelos.Comprador.models import Comprador
from Modelos.Proveedor.models import Proveedor
from  Modelos.Region.models import *

# Create your models here.
class Facturas(models.Model):
      CodigoOrdenCompra = models.CharField(blank=True, max_length=50)  # Listado/Codigo
      CodigoOrganismoComprador = models.CharField(max_length=80)
      compradorFk = models.ForeignKey(Comprador, null= True, blank= True, on_delete = models.CASCADE)
      CodigoProveedor = models.CharField(max_length=80)
      proveedorFk = models.ForeignKey(Proveedor, null=True, blank=True, on_delete=models.CASCADE)
      CodigoRegion = models.CharField(max_length=20)
      regionFk =  models.ForeignKey(Region, null=True, blank=True, on_delete=models.CASCADE)
      TotalFactura = models.FloatField()
      TotalDescuentos = models.FloatField(default=0)
      Fecha = models.DateField()
      EstadoExtraccion=models.BooleanField(default=False)

      def __str__(self):
            return self.CodigoOrdenCompra + ' - ' + self.CodigoProveedor + ' - ' + datetime.strftime(self.Fecha, '%Y-%m-%d')


