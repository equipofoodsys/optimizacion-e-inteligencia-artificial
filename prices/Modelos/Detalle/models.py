from django.db import models
from Modelos.Producto.models import Producto
from Modelos.Facturas.models import Facturas


# Create your models here.
class Detalle(models.Model):
    CodigoOrdenCompra = models.CharField(blank=True, max_length=50)  # Listado/Codigo
    facturaFk = models.ForeignKey(Facturas, null= True, blank= True, on_delete = models.CASCADE)
    CodigoProducto = models.CharField(max_length=50)
    productoFk = models.ForeignKey(Producto, null= True, blank= True, on_delete = models.CASCADE)
    PrecioNeto = models.FloatField(default=0)
    Cantidad = models.FloatField(default=0)