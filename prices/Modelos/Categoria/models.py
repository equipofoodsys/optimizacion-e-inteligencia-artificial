from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Categoria(models.Model):
    CodigoCategoria = models.CharField( max_length=50)
    DescripcionCategoria = models.CharField(max_length=100)
