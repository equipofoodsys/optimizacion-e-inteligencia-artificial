from django.db import models

# Create your models here.
class Formato(models.Model):
    CodigoFormato = models.CharField(unique=True, max_length=50)
    DescripcionFormato = models.CharField(max_length=100)
    UnidadesFormato = models.CharField(max_length=50)

