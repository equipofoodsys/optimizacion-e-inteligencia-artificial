from django.db import models
from django.core.urlresolvers import reverse
from Modelos.Region.models import *

# Create your models here.
class Proveedor(models.Model):
    CodigoProveedor = models.CharField(unique=True,max_length=80)
    NombreProveedor = models.CharField(blank=True,max_length=80)
    ActividadProveedor = models.CharField(blank=True,max_length=80)
    # CodigoSucursalProveedor = scrapy.Field()
    # NombreSucursalProveedor = scrapy.Field()
    RutSucursalProveedor = models.CharField(blank=True,max_length=80)
    DireccionProveedor = models.CharField(blank=True,max_length=100)
    # ComunaProveedor = scrapy.Field()
    regionPk = models.ForeignKey(Region, null= True, blank= True, on_delete = models.CASCADE)
    # PaisProveedor = scrapy.Field()
    NombreContactoProveedor = models.CharField(blank=True,max_length=80)
    CargoContactoProveedor = models.CharField(blank=True,max_length=60)
    FonoContactoProveedor = models.CharField(blank=True,max_length=80)
    MailContactoProveedor = models.CharField(blank=True,max_length=80)

    def __str__(self):
        """
        Cadena que representa a la instancia particular del modelo
        """
        return self.CodigoProveedor + ' , ' + self.NombreProveedor + ' , ' + self.ActividadProveedor + ' , ' + self.RutSucursalProveedor

    def get_absolute_url(self):
        """
        Devuelve el URL a una instancia particular de Provee
        """
        return reverse('food:proveedor-detail', args=[str(self.id)])