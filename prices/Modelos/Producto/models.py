from django.core.urlresolvers import reverse
from django.db import models
# from django.contrib.urls import reverse
from Modelos.Categoria.models import *
from Modelos.Formato.models import *
from Modelos.ListaBase.models import *

# Create your models here.
class Producto(models.Model):
    CodigoCategoria = models.CharField(blank=True,max_length=50)
    categoriaFk = models.ForeignKey(Categoria, null= True, blank= True, on_delete = models.CASCADE)
    CodigoProducto = models.CharField(max_length=50)
    DescripcionProducto = models.CharField(blank=True,max_length=80)
    PrecioCosto = models.FloatField(default=0)
    PrecioMin = models.FloatField(default=0)
    PrecioMedio = models.FloatField(default=0)
    PrecioMax = models.FloatField(default=0)
    PrecioProm = models.FloatField(default=0)
    FechaModifica = models.DateField()
    formatoFk = models.ForeignKey(Formato, null= True, blank= True, on_delete = models.CASCADE)
    Activo = models.BooleanField(default = True)
    listaBasepk = models.ForeignKey(ListaBase, null= True, blank= True, on_delete = models.CASCADE)

    # Moneda = scrapy.Field()
    # PrecioNeto = models.FloatField(default=0)
    # TotalDescuentos = models.FloatField(default=0)
    #

    def __str__(self):
        """
        Cadena que representa a la instancia particular del modelo
        """
        return self.CodigoProducto + ' , ' + self.DescripcionProducto

    def get_absolute_url(self):
        """
        Devuelve el URL a una instancia particular de Provee
        """
        return reverse('food:producto-detail', args=[str(self.id)])


