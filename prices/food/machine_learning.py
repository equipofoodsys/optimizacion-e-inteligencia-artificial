# -*- coding: utf-8 -*-

import codecs
import math
import time
from datetime import datetime,timedelta
from Modelos.Producto.models import Producto
from Modelos.Facturas.models import Facturas
from Modelos.Detalle.models import *
from  Modelos.Categoria.models import *
from  Modelos.Region.models import *
from Modelos.Formato.models import *
from Modelos.ListaBase.models import *
from Modelos.Proveedor.models import *
from Modelos.Comprador.models import *
from prices.settings import LEARNING_ROOT
import os.path

def add_des(kRegProd,xFile):
    try:
        fout = codecs.open(xFile, "r", encoding='utf-8', errors='ignore')
    except IOError:
        print('\nEerror al intentar abri el archivo: %s' % xFile)
    if fout:
        desReg = kRegProd.DescripcionC
        uniReg = kRegProd.UnidadesC
        # print('\n%s' % desReg)
        with fout as archivo:
            lines = archivo.readlines()
        siVa = True
        # print('\nLinea: %s' % lines)
        if len(lines)>0:
            for line in lines:
                kPedazos = (line.strip()).split(';')

                if len(kPedazos)>1:
                    kDes = kPedazos[0]
                    # print('\n%s' % kDes)
                    # kUni = kPedazos[1]
                    if kDes == desReg:
                        siVa = False
                        break
        if siVa:
            try:
                fout = codecs.open(xFile, "a", encoding='utf-8', errors='ignore')
                fout.writelines('\n%s;%s' % (desReg, uniReg))
            except IOError:
                print('\nEerror al intentar escribir en: %s' % xFile)

        fout.close()

def groups():
    xProd = ListaBase.objects.filter(estado=True).order_by('DescripcionC')
    if xProd:
        xGrupo = []
        try:
            xAcentos = {'Á': 'A', 'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U', 'Ü': 'U', 'Ñ': 'N'}
            for regProd in xProd:
                xDesc = (regProd.DescripcionC).strip()
                xPedazos = xDesc.split(' ')
                if len(xPedazos) > 2:
                    xNom = (xPedazos[0]).strip()
                    xNom = xNom.rstrip('S')
                    for l in xNom:
                        if l in xAcentos.keys():
                            xNom = xNom.replace(l, xAcentos[l])
                    if not xNom in xGrupo:
                        xGrupo.append(xNom)
        except UnicodeEncodeError:
            pass
        # print(xGrupo)
        if xGrupo:
            for file in xGrupo:
            # LEARNING_ROOT
                xFile = os.path.join(LEARNING_ROOT,file+'.dat')
                if not os.path.isfile(xFile):
                    try:
                        fout = open(xFile,'w')
                        fout.close()
                    except IOError:
                        print('\nError al intentar crear el archivo ==> %s' % xFile)
            for file in xGrupo:
                xWord = str(file.split('.'))
                # print('\nPalabra: %s ' % xWord)
                if len(xWord) > 2:
                    xProduct = str(xWord.strip("u'[']")) #[0]
                    xFile = os.path.join(LEARNING_ROOT, file+'.dat')
                    siExist = False
                    try:
                        fout = open(xFile, 'r')
                        fout.close()
                        siExist = True
                    except IOError:
                        print('\nError al intentar Abrir el archivo ==> %s' % xFile)
                    if siExist:
                        xProd = ListaBase.objects.filter(estado=True ) # DescripcionC = xProduct,
                        # print(xProd)
                        # fout = codecs.open(xFile, "r", encoding='utf-8', errors='ignore')
                        for xReg in xProd:
                            xPedazos = (xReg.DescripcionC).strip()
                            xPedazos = xPedazos.split(' ')
                            # print('\n%s ' % xPedazos)
                            if len(xPedazos)>2:
                                yWord = xPedazos[0]
                                # print('\nyWord = %s ==> xProduct= %s ' % (yWord,xProduct))
                                if yWord == xProduct:
                                    add_des(xReg,xFile)


def Clasificar(DesSC):
    FileList = os.listdir(LEARNING_ROOT)
    print DesSC
    products = []
    xNewDesc = ''
    xNewUni = ''
    for elemento in FileList:
        archivo = os.path.join(LEARNING_ROOT,elemento)
        if os.path.isfile(archivo):
            xProd = elemento.split('.')
            yProd = str(xProd[0])
            if not yProd in products:
                products.append(yProd)
    if products:
        yDesc = DesSC.replace('-','')
        yDesc = yDesc.upper()
        xWords = pedazos = yDesc.split(' ')
        try:
            xAcentos = {'Á': 'A', 'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U', 'Ü': 'U', 'Ñ': 'N'}
            for pos,palabra in enumerate(pedazos):
                nueva = ''
                # print ('\n%s' % palabra)
                for l in palabra:
                    if l in xAcentos.keys():
                        nueva = palabra.replace(l, xAcentos[l])
                        # print nueva
                        break
                if len(nueva):
                    pedazos[pos]=nueva
        except UnicodeEncodeError:
            pass
        # print pedazos
        xFile = ''
        for des in products:
            if des in pedazos:
                xFile = des
        if len(xFile):
            # print('\nBuscaremos en el archivo %s ' % xFile)
            yFile = os.path.join(LEARNING_ROOT, xFile+'.dat')
            if os.path.isfile(yFile):
                try:
                    with codecs.open(yFile, "r", encoding='utf-8', errors='ignore') as archivo:
                        lines = archivo.readlines()
                    xmax = -1
                    for line in lines:
                        pedazos = line.split(';')
                        if len(pedazos)>1:
                            yyDes = pedazos[0]
                            print yyDes
                            yyUni = pedazos[1]
                            xPlabras = yyDes.split(' ')
                            son = len(xPlabras)
                            tiene = 0

                            for p in xPlabras:
                                if p in xWords:
                                    tiene += 1
                            print tiene
                            if tiene >= son:
                                xNewDesc = yyDes
                                xNewUni = yyUni
                                break
                            else:
                                if tiene > xmax:
                                    xmax = tiene
                                    xNewDesc = yyDes
                                    xNewUni = yyUni
                    # fout.close()
                except IOError:
                    print('\nError al intentar crear el archivo ==> %s' % xFile)
    print xNewDesc +' --- ' +xNewUni
    yes = False
    if len(xNewDesc)>1:
        yes = True
    return xNewDesc,xNewUni,yes