import random
import operator
import time
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib import messages
import codecs

from django.shortcuts import render

from Modelos.Comprador.models import Comprador
from Modelos.Proveedor.models import Proveedor
from Modelos.Producto.models import Producto
from Modelos.Facturas.models import Facturas
from Modelos.Detalle.models import *
from  Modelos.Categoria.models import *
from  Modelos.Region.models import *
from Modelos.Formato.models import *
from Modelos.ListaBase.models import *

from django.views import generic

from food.Extraccion import *
import json

from datetime import datetime,timedelta

# Para los formularios
from django.template import RequestContext
from food.forms import ConsultaProveedorForms,ConsultaFacturasForms, ListarOrdenesExtraidasxFechaForms
from django.shortcuts import render_to_response, redirect, HttpResponseRedirect


# Create your views here. unicode(xDesSC, errors='ignore')





def index(request):
     # Genera contadores de algunos de los objetos principales
    num_proveedores = Proveedor.objects.all().count()
    num_compradores = Comprador.objects.all().count()
    num_detalles = Producto.objects.all().count()
    image_logo = open("food/images/Logo_FoodSys.png", "rb").read()
    #  Lanzaremos aqui los hilos para extraccion automatica (tambien actualizar lista de precios sugeridos)

    # Renderiza la plantilla HTML index.html con los datos en la variable contexto
    return render(
        request,
        'index.html',
        context={'num_proveedores': num_proveedores, 'num_compradores': num_compradores, 'num_detalles': num_detalles, 'image_logo':image_logo},
    )


def detalle_ordenescompra(request,orden):
    regFactura = Facturas.objects.filter(id = orden, EstadoExtraccion=True)
    # p = request.GET.get('page')
    # print('\nget_page: %s  ===  \n' % p)
    if regFactura:
        detalle = []
        regFactura = regFactura[0]
        regDetalle = Detalle.objects.filter(CodigoOrdenCompra = regFactura.CodigoOrdenCompra)
        contexto = {'factura': regFactura, 'detalle':''}
        if regDetalle:
            xdetalle = Detalle()
            for regd in regDetalle:
                detalle.append(regd)
            contexto = {'factura': regFactura, 'detalle':detalle}
    else:
        contexto = {}
    return render(request, 'DetallesOrdenCompra/detalle_ordenescompra.html', contexto)

def request_page(request):
    xurlbase='http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?'
    ticket='&ticket=C74E17AB-BB1C-4CD2-9495-C55A5D2A2E56'
    urlbase = 'http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?'
    urlbase += 'codigo='
    tTotalFacturas = 0
    dib = ['|', '/', '-', '|', '\\']
    contexto = {'num': 0}
    tiempo = [-1,0]
    if (request.GET.get('boton')):
        xIntentos = True

        if len(ListaBase.objects.all())>1: #.order_by('Fecha')
            Nerror = 0
            # fout = codecs.open('files/PorClasificar'+str(len(Facturas.objects.filter(EstadoExtraccion=False)))+'.txt', "w",encoding='utf-8', errors='ignore')
            while xIntentos:
                facturas = Facturas.objects.filter(EstadoExtraccion=False).order_by('Fecha') # Solo las facturas que aun no se han extraido
                # tTotalFacturas = len(facturas)
                xIntentos = xIntentos + 1
                cExtract = 0
                kInd = -1
                if facturas:
                    for orden in facturas:
                        url = urlbase+orden.CodigoOrdenCompra.replace("\n","")+ticket
                        xFecha = orden.Fecha
                        xOrdenCompra = orden.CodigoOrdenCompra
                        if Nerror >= 10:  # tiempo = [-1,0]  connect and read
                            if Nerror % 10:
                                tiempo[0] += 5
                                tiempo[1] = 10
                            Json, code = Scrapy(url,tiempo)
                        else:
                            Json, code = Scrapy(url, tiempo)
                        if code != -1:
                            Nerror += 1
                            # print('\nError code = %s  Nerrores %s' % (code,Nerror))
                        # print('\nURL BAse: %s' % url)
                        # print('\nJson = %s ' % Json)
                        if Json:
                            # kInd = kInd + 1
                            # if kInd >= len(dib):
                            #     kInd = 0
                            # mensa = 'Proccess {}'.format(dib[kInd])
                            # messages.success(request, mensa)
                            cExtract = cExtract + 1
                            regReg = Region.objects.filter( DescripcionRegion = (Json['Listado'][0]['Comprador']['RegionUnidad']).replace("\n","") ) #Region
                            if not regReg:
                                region = Region()
                                region.DescripcionRegion = (Json['Listado'][0]['Comprador']['RegionUnidad']).replace("\n","")
                                region.CodigoRegion = '{}'.format(len(Region.objects.all()))
                                region.save()
                                idregion=region.id
                            else:
                                idregion = regReg[0].id
                            regFact = Facturas.objects.filter(CodigoOrdenCompra = orden.CodigoOrdenCompra) # Facturas
                            if regFact:
                                region = ''
                                regFactura = regFact[0]
                                regFactura.CodigoOrganismoComprador = Json['Listado'][0]['Comprador']['CodigoOrganismo']
                                regFactura.CodigoRegion =  '{}'.format(idregion) #Json['Listado'][0]['Comprador']['RegionUnidad']#RegionUnidad
                                regFactura.TotalFactura = Json['Listado'][0]['TotalNeto']
                                regFactura.EstadoExtraccion = True
                                xSave = False # Solo Comprador
                                regComprador = Comprador.objects.filter(CodigoOrganismoComprador = Json['Listado'][0]['Comprador']['CodigoOrganismo']) # Comprador
                                if not regComprador:
                                    regComprador = Comprador()
                                    regComprador.CodigoOrganismoComprador = Json['Listado'][0]['Comprador']['CodigoOrganismo']
                                    regComprador.NombreOrganismoComprador = Json['Listado'][0]['Comprador']['NombreOrganismo']
                                    regComprador.RutUnidadComprador = Json['Listado'][0]['Comprador']['RutUnidad']
                                    regComprador.ActividadComprador = Json['Listado'][0]['Comprador']['Actividad']
                                    regComprador.DireccionUnidadComprador = Json['Listado'][0]['Comprador']['DireccionUnidad']
                                    regComprador.NombreContactoComprador = Json['Listado'][0]['Comprador']['NombreContacto']
                                    regComprador.CargoContactoComprador = Json['Listado'][0]['Comprador']['CargoContacto']
                                    if len(Json['Listado'][0]['Comprador']['FonoContacto'][:79])>80:
                                        xContact = Json['Listado'][0]['Comprador']['FonoContacto'][:79]
                                    else:
                                        xContact = Json['Listado'][0]['Comprador']['FonoContacto']
                                    regComprador.FonoContactoComprador = xContact
                                    regComprador.MailContactoComprador = Json['Listado'][0]['Comprador']['MailContacto']
                                    xSave = True
                                else:
                                    regComprador = regComprador[0]
                                regReg = Region.objects.filter(DescripcionRegion=(Json['Listado'][0]['Proveedor']['Region']).replace("\n", ""))
                                if not regReg:
                                    region = Region()
                                    region.DescripcionRegion = (Json['Listado'][0]['Proveedor']['Region']).replace("\n", "")
                                    region.CodigoRegion = '{}'.format(len(Region.objects.all()))
                                    region.save()
                                else:
                                    region = regReg[0]
                                regProveedor = Proveedor.objects.filter(CodigoProveedor=Json['Listado'][0]['Proveedor']['Codigo'])  # Proveedor
                                if not regProveedor:
                                    regProveedor = Proveedor()
                                    regProveedor.CodigoProveedor = Json['Listado'][0]['Proveedor']['Codigo']
                                    regProveedor.NombreProveedor = Json['Listado'][0]['Proveedor']['Nombre']
                                    regProveedor.ActividadProveedor = Json['Listado'][0]['Proveedor']['Actividad']
                                    regProveedor.RutSucursalProveedor = Json['Listado'][0]['Proveedor']['RutSucursal']
                                    regProveedor.DireccionProveedor = Json['Listado'][0]['Proveedor']['Direccion']
                                    regProveedor.NombreContactoProveedor = Json['Listado'][0]['Proveedor']['NombreContacto']
                                    regProveedor.CargoContactoProveedor = Json['Listado'][0]['Proveedor']['CargoContacto']
                                    regProveedor.FonoContactoProveedor = Json['Listado'][0]['Proveedor']['FonoContacto'][:79]
                                    regProveedor.MailContactoProveedor = Json['Listado'][0]['Proveedor']['MailContacto']
                                    regProveedor.regionPk = region
                                    regProveedor.save()
                                else:
                                    regProveedor = regProveedor[0]
                                if xSave: # Solo Comprador
                                    regComprador.regionPk = region
                                    regComprador.save()
                                # Actualizar Campo de llave foranea de Facturas
                                regFactura.compradorFk = regComprador
                                regFactura.proveedorFk = regProveedor
                                regFactura.regionFk = region
                                xSaveFactura = False # Si hay registros en detalle Guardar Factura
                                # regFactura.save() # Almacena en DB las Ordenes de Compra  Pero aqui no debe ir
                                for item in Json['Listado'][0]['Items']['Listado']:
                                    cont = getCatego((item['Categoria']).strip())
                                    # xDesProd = item['EspecificacionComprador']
                                    if  cont: #xDesProd.find('PARA')<=0:
                                        xSaveFactura = True # Hay al menos un registro
                                        xDesProd = item['EspecificacionComprador']
                                        if (item['EspecificacionComprador']).find('(') > -1:
                                            if (item['EspecificacionComprador']).find('(') < 5:
                                                if (item['EspecificacionComprador']).find(')') > -1:
                                                    xDesProd = (item['EspecificacionComprador']).split(')')[1]
                                        xDesProd = xDesProd.strip(',.:;-+= ')
                                        if xDesProd.find('RM')>0:
                                            xDesProd = xDesProd[0:xDesProd.find('RM')]
                                        if xDesProd.find('REGION') > 0:
                                            xDesProd = xDesProd[0:(xDesProd.find('REGION')-2)]
                                        xxDesProd = xDesProd.rstrip(',.:;-+= V IV')
                                        if len(xDesProd) > 80:
                                            xxDesProd = xDesProd[0:79]
                                        xDesProd,xUni,xListaBase = clasifica(xxDesProd)
                                        xClasificado = False
                                        if xDesProd != '=':
                                            xClasificado = True
                                        else:
                                            xxDesProd = xxDesProd.strip(' \n\t\r')
                                            if not xxDesProd in ListaBase.objects.filter(DescripcionSC=xxDesProd , estado = False):
                                                regListaBase = ListaBase()
                                                regListaBase.estado = False
                                                regListaBase.DescripcionSC = xxDesProd
                                                # Standariza descripcion del producto por Apredizaje automatico
                                                regListaBase.DescripcionC, regListaBase.UnidadesC, kSw = Clasificar(
                                                    xxDesProd)
                                                if kSw:
                                                    yxListaBase = ListaBase.objects.filter(
                                                        DescripcionC=regListaBase.DescripcionC)  # estado=True)
                                                    if yxListaBase:
                                                        regListaBase.productopk = yxListaBase[0]
                                                    else:
                                                        regListaBase.productopk = None
                                                    regListaBase.save()
                                                    xListaBase = regListaBase
                                                else:
                                                    # Standariza descripcion del producto por semi-clasificador
                                                    regListaBase.DescripcionC, regListaBase.UnidadesC = std_producto(
                                                        xxDesProd)
                                                    yxListaBase = ListaBase.objects.filter(
                                                        DescripcionC=regListaBase.DescripcionC)  # estado=True)
                                                    if yxListaBase:
                                                        regListaBase.productopk = yxListaBase[0]
                                                    else:
                                                        regListaBase.productopk = None
                                                    regListaBase.save()
                                                    xListaBase = regListaBase
                                            else:
                                                yxListaBase = ListaBase.objects.filter(DescripcionSC=xxDesProd, estado=True)
                                                if not yxListaBase:
                                                    yxListaBase = ListaBase.objects.filter(DescripcionSC = xxDesProd, estado = False)
                                                if yxListaBase:
                                                    xListaBase = yxListaBase[0]
                                                else:
                                                    xListaBase = None
                                            xDesProd = xxDesProd

                                        regProducto = Producto.objects.filter(DescripcionProducto = xDesProd)
                                        if not regProducto:
                                            regProducto = Producto()
                                                # regProducto.CodigoCategoria = str(item['CodigoCategoria']).strip() #item['CodigoCategoria']
                                            cad = xDesProd.split()
                                            if not xClasificado:
                                                if len(cad)>0:
                                                    if cad[0].find(')')>-1:
                                                        xcad = cad[0].split(')')
                                                        if len(xcad)>0:
                                                            cad = xcad[1]
                                                        else:
                                                            cad = '0000'
                                                    else:
                                                        cad = cad[0]
                                                else:
                                                    cad = '00002'
                                            else:
                                                cad = cad[0]

                                            xCodProducto = regProducto.CodigoProducto = cad +'-'+'{}'.format(len(Producto.objects.all()))
                                            regProducto.DescripcionProducto = xDesProd   #yDes
                                            # if not xClasificado:
                                            #     # Determinado las Unidades del Producto
                                            #     xUnidades = ['BANDEJA','KILO','UNIDAD','CAJA','MALLA','BOLSA','KG','PAQUETE','GRANEL','GRANDE','UNIDADES','ATADO','K','PAQ','1K']
                                            #     xMayor = ['BANDEJA','CAJA','MALLA','BOLSA','PAQUETE','GRANEL','GRANDE','UNIDADES','ATADO','PAQ']
                                            #     kilos = ['K','KG','1K','1-k','KILOGRAMO']
                                            #     xMenoreo = ['GR']
                                            #     xDes=xDesProd.split()
                                            #     Des =[]
                                            #     Uni = 'Desconocidas'
                                            #     xCodFormato = 'NN'
                                            #     for x in xDes:
                                            #         x = x.strip(',.:;-+= ')
                                            #         Des.append(x)
                                            #     for u in Des:
                                            #         if u in xUnidades:
                                            #             pos = Des.index(u)
                                            #             xCodFormato = u + '-'
                                            #             if u in xMayor:
                                            #                 if (u == 'PAQUETE') or (u == 'ATADO') or ('PAQ'):
                                            #                     Uni = xDes[pos]
                                            #                     i = 0
                                            #                     while (i<3) and (pos < (len(xDes)-1)):
                                            #                         i = i +1
                                            #                         pos = pos +1
                                            #                         if xDes[pos] in ['UNIDAD','UNIDADES']:
                                            #                             Uni = Uni + ' ' + xDes[pos]
                                            #                             break
                                            #                         else:
                                            #                             Uni = Uni + ' ' + xDes[pos]
                                            #                 elif u == 'UNIDADES':
                                            #                     Uni = xDes[pos-1] + ' ' + xDes[pos]
                                            #                 elif u == 'BOLSA':
                                            #                     Uni = xDes[pos]
                                            #                     i = 0
                                            #                     while (i < 2) and (pos < (len(xDes) - 1)):
                                            #                         i = i + 1
                                            #                         pos = pos + 1
                                            #                         if xDes[pos] in ['UNIDAD','UNIDADES']:
                                            #                             Uni = Uni + ' ' + xDes[pos]
                                            #                             break
                                            #                         else:
                                            #                             Uni = Uni + ' ' + xDes[pos]
                                            #                 else:
                                            #                     Uni = xDes[pos]
                                            #                     i = 0
                                            #                     while (i < 2) and (pos < (len(xDes) - 1)):
                                            #                         i = i + 1
                                            #                         pos = pos + 1
                                            #                         Uni = Uni + ' ' + xDes[pos]
                                            #             else:
                                            #                 if u == 'UNIDAD':
                                            #                     Uni = u
                                            #                     for gr in Des:
                                            #                         if gr in xMenoreo:
                                            #                             i = 0
                                            #                             while (i < 2) and (pos < (len(xDes) - 1)):
                                            #                                 i = i + 1
                                            #                                 pos = pos + 1
                                            #                                 Uni = Uni + ' ' + xDes[pos]
                                            #                             break
                                            #                 else:
                                            #                     if u in kilos:
                                            #                         u = 'KILO'
                                            #                     Uni = u
                                            #             break
                                            #         xUni = Uni
                                            if xClasificado:
                                                Uni = xUni
                                                # u = xUni.split()
                                                xCodFormato = 'F-'
                                            else:
                                                Uni = 'Formato Sin Clasificar'
                                            regFormato = Formato.objects.filter( DescripcionFormato = Uni)
                                            if not regFormato:
                                                if xClasificado:
                                                    regFormato = Formato()
                                                    regFormato.CodigoFormato = xCodFormato + '{}'.format(len(Formato.objects.all()))
                                                    regFormato.DescripcionFormato = Uni
                                                    regFormato.UnidadesFormato = Uni
                                                    regFormato.save()
                                                else:
                                                    # regFormato.DescripcionFormato = Uni
                                                    regFormato = None
                                            else:
                                                regFormato = regFormato[0]
                                            regProducto.formatoFk = regFormato
                                            regProducto.PrecioCosto = 0;
                                            regProducto.PrecioMin = 0;
                                            regProducto.PrecioMedio = 0
                                            regProducto.PrecioMax = item['PrecioNeto']
                                            regProducto.FechaModifica = xFecha
                                            regProducto.Activo = True
                                            xCategoria = 'C-D'
                                            if str(item['CodigoCategoria']).strip():
                                                xCategoria = str(item['CodigoCategoria']).strip()
                                            regCategoria = Categoria.objects.filter(CodigoCategoria = xCategoria)
                                            if not regCategoria:
                                                regCategoria = Categoria()
                                                regCategoria.CodigoCategoria = xCategoria
                                                if (item['Categoria']).strip():
                                                    regCategoria.DescripcionCategoria = (item['Categoria']).strip()[:79]
                                                else:
                                                    regCategoria.DescripcionCategoria = 'Categoria Desconocida'
                                                regCategoria.save()
                                            else:
                                                regCategoria = regCategoria[0]
                                            regProducto.CodigoCategoria = xCategoria
                                            regProducto.categoriaFk = regCategoria
                                            regProducto.listaBasepk = xListaBase
                                            if not xClasificado:
                                                regProducto.Activo = False
                                            regProducto.save()
                                        else:
                                            xCodProducto = regProducto[0].CodigoProducto
                                            regProducto = regProducto[0]
                                        regDetalle = Detalle()
                                        regDetalle.CodigoOrdenCompra = xOrdenCompra
                                        regDetalle.CodigoProducto = xCodProducto
                                        regDetalle.PrecioNeto = item['PrecioNeto']
                                        regDetalle.Cantidad = item['Cantidad']
                                        regDetalle.productoFk = regProducto
                                        # regFactura.save()
                                        regDetalle.facturaFk = regFactura
                                        regDetalle.save()


                                            # fout.writelines('%s\n' % xxDesProd)
                                if xSaveFactura:
                                    regFactura.save()
                                else:
                                    if regFactura:
                                        regFactura.delete()
                if len(Facturas.objects.filter(EstadoExtraccion=False)) >= 1:
                    xIntentos = True
                    if Nerror >= 100:
                        xIntentos = False
                else:
                    xIntentos = False
                    # xval = tTotalFacturas - quedan
                    # contexto = {'val': xval , 'num': 0, 'son' : tTotalFacturas}
                    # return render(request, 'extraccionfacturas/extraccion_facturas.html', contexto)
                # if tTotalFacturas<=0:
                #      xIntentos = 100
                # else:
                #     xIntentos = xIntentos + 1
                # messages.success(request,'En bucle {} se logro extraer {}'.format(xIntentos,cExtract))
            quedan = len(Facturas.objects.filter(EstadoExtraccion=False))
            xval = quedan
            # fout.close()
            contexto = {'val': xval, 'num': 0, 'son': tTotalFacturas}
            return render(request, 'extraccionfacturas/extraccion_facturas.html', contexto)
        else:
            contexto = {'val': -1, 'num': -1, 'son': 'No Existe Lista Base!!! Cree Lista-Base e intente de Nuevo!!!!'}
            return render(request, 'extraccionfacturas/extraccion_facturas.html', contexto)
    else:
        if (request.GET.get('stop')):
            contexto = {'val': -1, 'num': 0, 'son': tTotalFacturas}
            return render(request, 'extraccionfacturas/mensajes.html', contexto)
        else:
            contexto = {'val':0,'num':0, 'son' : tTotalFacturas}
    return render(request, 'extraccionfacturas/extraccion_facturas.html', contexto)

def ListarOrdenesCompraxFecha(request):
    if request.method == "POST":
        form = ListarOrdenesExtraidasxFechaForms(request.POST or None)
        if form.is_valid():
            fechaI = form.cleaned_data['FechaInicio']
            fechaF = form.cleaned_data['FechaFin']
            if fechaI <= fechaF:
                rangodias = []
                undia = timedelta(days=1)
                while fechaI <= fechaF:
                    if datetime.weekday(fechaI) in [0, 1, 2, 3, 4]:
                        rangodias.append(datetime.strftime(fechaI, '%Y-%m-%d'))
                    fechaI = fechaI + undia
                ordenes = []
                for fecha in rangodias:
                    regFactura = Facturas.objects.filter(Fecha=datetime.strptime(fecha, '%Y-%m-%d'),EstadoExtraccion = True)
                    if regFactura:
                        for factura in regFactura:
                            ordenes.append(factura)
                contexto = {'ordenes': ordenes}
                return render(request, 'DetallesOrdenCompra/lista_ordenesxfecha.html', contexto)
            else:
                return render(request, 'DetallesOrdenCompra/listar_ordenes.html', {'form': form, 'Err':'Fecha Inicio debe ser Menor a Fecha Fin'})

    else:
        form = ListarOrdenesExtraidasxFechaForms()
        # objeto = request.GET.get('orden')
        # if objeto:
        #     print('\nValores leidos: \n')
        #     for i in objeto:
        #         print(i)


        #     contexto = {'ordenes': request.GET.get('orden')}
        #     return render(request, 'DetallesOrdenCompra/lista_ordenesxfecha.html', contexto)
    return render(request, 'DetallesOrdenCompra/listar_ordenes.html', {'form': form})


def ExtraerOrdenesCompra(request):
    # print('\nEstamos enextraeer porveedor\n')
    if request.method == "POST":
        xTag =''
        form = ConsultaProveedorForms(request.POST or None)
        if form.is_valid():
            fechaI = form.cleaned_data['FechaInicio']
            fechaF = form.cleaned_data['FechaFin']
            codigos = [] #form.cleaned_data['codigo-0']
            xCod = []
            Texcodigos = form.cleaned_data['CodigosProveedor']
            if len(Texcodigos):
                xCod =Texcodigos.rstrip().split('\r\n')
                for i in xCod:
                    codigos.append(str(i))
            # print(len(codigos))
            if len(form.cleaned_data)>3:
                for i in form.cleaned_data:
                    if i == 'FechaInicio':
                        fechaI = form.cleaned_data[i]
                    if i == 'FechaFin':
                        fechaF = form.cleaned_data[i]
                    if not i in ['FechaFin','FechaInicio']:
                       if form.cleaned_data[i]:
                           if not form.cleaned_data[i] in codigos:
                                codigos.append(str((form.cleaned_data[i])[0]))
            si = False
            try:
                listabase = open('files/codigoprov.txt', "r")
                tt = listabase.read()
                listabase.close()
                si = True
            except IOError:
                si = False
            if si:
                with codecs.open('files/codigoprov.txt', 'r', encoding='utf-8', errors='ignore') as archivo:
                    lines = archivo.readlines()
                for linea in lines:
                    yCod = str(linea.strip(' \n\r\t'))
                    if not yCod in codigos:
                        codigos.append(yCod)
            # print('\nCidgos: %s' % codigos)
            if (fechaI <= fechaF)and(len(codigos)):
                xurlbase = 'http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?'
                ticket = '&ticket=C74E17AB-BB1C-4CD2-9495-C55A5D2A2E56'
                rangodias = []
                undia = timedelta(days=1)
                while fechaI <= fechaF:
                    if datetime.weekday(fechaI) in [0, 1, 2, 3, 4]:
                        rangodias.append(datetime.strftime(fechaI, '%Y-%m-%d'))
                    fechaI = fechaI + undia
                codprov = codigos #.rstrip().split('\r\n')
                # codigos = []
                tFact = len(Facturas.objects.filter(EstadoExtraccion=False).order_by('Fecha'))
                i = 0
                tiempo = [-1, 0]
                for xcod in codprov:
                    if len(xcod)>2:
                        for fecha in rangodias:  #datetime.strptime(,'%Y-%m-%d')
                            reg = Facturas.objects.filter(CodigoProveedor=xcod,Fecha=datetime.strptime(fecha,'%Y-%m-%d'))
                            # print('\n %s ' % reg)
                            if not reg:
                                urlbase = xurlbase
                                a, m, d = fecha.split('-')
                                urlbase += 'fecha='
                                urlbase += d + m + a
                                codprov = xcod.replace(" ", "")
                                urlbase += '&CodigoProveedor=' + codprov + ticket
                                # return render(request, 'extraccionfacturas/mensajes.html')
                                # print('\nURL BAse: %s' % urlbase)
                                Json,code = Scrapy(urlbase,tiempo)
                                # print('\nCode = %s' % code)
                                # print('\nJson = %s ' % Json)

                                if Json:
                                    for Data in Json['Listado']:
                                        # codigos.append(Data['Codigo'])

                                        if len(Facturas.objects.filter(CodigoOrdenCompra=Data['Codigo']))<=0:
                                            i += 1
                                            orden = Facturas(CodigoOrdenCompra=Data['Codigo'], CodigoProveedor='%s' % xcod,
                                                         Fecha=datetime.strptime(fecha, '%Y-%m-%d'), TotalFactura=0,
                                                         EstadoExtraccion=False)
                                            orden.save()

                facturas = Facturas.objects.filter(EstadoExtraccion=False).order_by('Fecha')
                contexto = {'ordenes': facturas, 'tFactAntes' : tFact, 'tExtraidas' : i}
                return render(request, 'extraccionfacturas/listar_ordenes.html', contexto)
    else:
        form=ConsultaProveedorForms()
    return render(request, 'extraccionproveedor/extraccion_proveedor.html', {'form': form})

def lista_ordenes(request):
    facturas=Facturas.objects.filter(EstadoExtraccion=False).order_by('Fecha')
    contexto = {'ordenes':facturas, 'tFactAntes' : len(facturas), 'tExtraidas' : -1}
    return render(request,'extraccionfacturas/listar_ordenes.html',contexto)



def ExtraerFacturas(request):
    facturas = Facturas.objects.filter(EstadoExtraccion=False).order_by('Fecha')
    nFact = 0
    if facturas: # http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?codigo=539119-4213-SE18&ticket=C74E17AB-BB1C-4CD2-9495-C55A5D2A2E56
        nFact = len(facturas)
    contexto = {'num': nFact, 'val' : 0}
    # messages.success(request, ' *****  Hola *************')
    return render(request, 'extraccionfacturas/extraccion_facturas.html',contexto)

def listaproductografica(request):
    op = request.GET.get('opcion')
    # print('\nOpcion: %s\n' % op)
    productos = Producto.objects.filter(Activo = True).order_by('DescripcionProducto')
    if not productos:
        contexto = {}
    else:
        contexto = {'productos':productos,'op' : op}
    return render(request, 'Graficas/ver_productos.html', contexto)


    # CodigoOrdenCompra = models.CharField(blank=True, max_length=50)  # Listado/Codigo
    # facturaFk = models.ForeignKey(Facturas, null= True, blank= True, on_delete = models.CASCADE)
    # CodigoProducto = models.CharField(max_length=50)
    # productoFk = models.ForeignKey(Producto, null= True, blank= True, on_delete = models.CASCADE)
    # PrecioNeto = models.FloatField(default=0)
    # Cantidad = models.FloatField(default=0)


def productgraphic(request,pk):
    cProduct = request.GET.get('codigo')
    precios = Detalle.objects.filter(CodigoProducto = cProduct).order_by('PrecioNeto')
    if precios:
        xDes_Prod = precios[0].productoFk.DescripcionProducto
        xFormato = precios[0].productoFk.formatoFk.UnidadesFormato
        color = []
        valores = {}
        tTotalFacturas = {}
        for item in precios:
            if item.productoFk.Activo:
                if not item.PrecioNeto in valores:
                    valores.update({item.PrecioNeto : item.Cantidad})
                    r = lambda: random.randint(0, 255)
                    color.append('#%02X%02X%02X' % (r(), r(), r()))
                else:
                     valores[item.PrecioNeto]  += item.Cantidad
                if not item.facturaFk.id in tTotalFacturas:
                    tTotalFacturas.update({item.facturaFk.id:1})
                else:
                    tTotalFacturas[item.facturaFk.id]+=1
        i = len(color)
        p = sorted(valores.items(), key=operator.itemgetter(0))
        # print('\nDatos: %s' % p)
        precios = []
        demanda = []
        for pr,d in p:
            precios.append(pr)#"{0:.2f}".format(p))
            demanda.append(d)#"{0:.1f}".format(d))
        # print('\nDatos: %s' % p)

        sTotal = 0
        pmFact = 0
        radio = []
        tVentas = [a*b for a,b in zip(precios, demanda)]
        for e in tVentas:
            sTotal += e
        for v in tVentas:
           radio.append("{0:.1f}".format((v*100)/sTotal))
        for a in tVentas:
            pmFact = pmFact + a
        pmFact = "{0:.2f}".format(pmFact/len(tTotalFacturas))
        color = json.dumps(color)
        precios = json.dumps(precios)
        demanda = json.dumps(demanda)
        context = {
                'producto': xDes_Prod,
                'precios': precios,
                'demandas': demanda,
                'volumen' : json.dumps(tVentas),
                'color': color,
                'datos': p,
                'i': i,
                'ventas' : tVentas,  #  tVentas = [ 125 , 256 , 3568 ]
                'formato' : xFormato,
                'promfact' : pmFact,
                'tfact' : len(tTotalFacturas),
                'radio' : json.dumps(radio),
                'op' : 1
            }
    else:
        context = {}
    return render(request, 'Graficas/Graphic_product.html', context)


def top_ten(request):
    detalle = Detalle.objects.all()
    context = {'val': 0}
    if detalle:
        tTotalFacturas = {}
        xtop = {}
        tBruto = {}
        for regDet in detalle:
            if regDet.productoFk.Activo:
                if not regDet.productoFk in xtop:
                    xtop.update({regDet.productoFk:regDet.Cantidad})
                    tTotalFacturas.update({regDet.productoFk:1})
                    tBruto.update({regDet.productoFk:regDet.PrecioNeto*regDet.Cantidad})
                else:
                    xtop[regDet.productoFk] += regDet.Cantidad
                    tTotalFacturas[regDet.productoFk] += 1
                    tBruto[regDet.productoFk] += (regDet.PrecioNeto*regDet.Cantidad)
        if xtop:
            top = sorted(xtop.items(), key=operator.itemgetter(1),reverse=True)
            top = top[0:10]
            pmFact = 0
            datos = []
            label = []
            color = []
            xTotal = []
            nFact = 0
            tVentas = []
            sTotal = 0
            for a,b in top:
                regD = Detalle()
                regD.productoFk = a
                regD.Cantidad = b
                regD.PrecioNeto = tTotalFacturas[a]
                nFact += tTotalFacturas[a]
                pmFact += b*tBruto[a]
                sTotal += b
                tVentas.append(b*tBruto[a])
                xTotal.append(regD.Cantidad)
                etiq = regD.productoFk.DescripcionProducto
                if etiq.find('-')>1:
                    tDes = (regD.productoFk.DescripcionProducto).split('-')
                    etiq = tDes[1]
                else:
                    etiq = etiq[0:15]
                label.append(etiq)
                datos.append(regD)
                r = lambda: random.randint(0, 255)
                color.append('#%02X%02X%02X' % (r(), r(), r()))
            radio = []
            for v in tVentas:
                radio.append("{0:.1f}".format((v * 100) / pmFact))

            x = range(0, 10)
            y = json.dumps(xTotal)
            context = {
                    'producto': datos,
                    'color': color,
                    'x': x,
                    'y': y,
                    'i': 10,
                    'promfact': '{0:.2f}'.format(pmFact/nFact),
                    'etiketas' : json.dumps(label),
                    'radio': json.dumps(radio),
                    'volumen' : json.dumps(tVentas)
                  }
    else:
        context = {'val' : 0}
    return render(request, 'Graficas/Top_Ten_Graphic.html', context)

def mayor_segmentacion(request):
    detalle = Detalle.objects.all()
    context = {'val': 0}
    if detalle:
        data = {}
        xtop = {}
        xdata={}
        for regDet in detalle:
            if regDet.productoFk.Activo:
                if not regDet.productoFk in data:
                    xtop.update({regDet.productoFk: 1})
                    data.update({regDet.productoFk: []})
                    data[regDet.productoFk].append({'nprecios': 0, 'precio': [], 'cant': []})
                # else:
                for k in data[regDet.productoFk]:
                    if not regDet.PrecioNeto in k['precio']:
                        k['precio'].append(regDet.PrecioNeto)
                        k['cant'].append(regDet.Cantidad)
                        k['nprecios'] += 1
                        xtop[regDet.productoFk] += 1
                    else:
                        k['cant'][[j for j, val in enumerate(k['precio']) if val == regDet.PrecioNeto][0]] += regDet.Cantidad

        if xtop:
            # print('\n Numeo de registros: %s ' % len(xtop))
            top = sorted(xtop.items(), key=operator.itemgetter(1), reverse=True)
            # top = dict(top)
            datos = []
            label = []
            color = []
            xTotal = []
            nFact = 0
            tVentas = []
            sTotal = 0
            solo = 0
            for k,v in top:
                xdata.update({k:[]})
                xdata[k].append(data[k])
                regD = Detalle()
                regD.productoFk = k
                xVenta = 0
                for item in data[k]:
                    regD.Cantidad = item['nprecios']
                    for j, val in enumerate(item['precio']):
                        xVenta += item['precio'][j]*item['cant'][j]
                tVentas.append(xVenta)
                sTotal += xVenta
                xTotal.append(regD.Cantidad)
                datos.append(regD)
                etiq = regD.productoFk.DescripcionProducto
                tDes = []
                if etiq.find('-') > 1:
                    # etiq = etiq[0:etiq.find('-')]
                    tDes = (regD.productoFk.DescripcionProducto).split('-')
                    etiq = tDes[1]
                else:
                    etiq = etiq[0:15]
                label.append(etiq)
                r = lambda: random.randint(0, 255)
                color.append('#%02X%02X%02X' % (r(), r(), r()))
                solo += 1
                if solo>=10:
                     break
                          # print(val)
            # print('\nNum Reg tVentas %s ' % tVentas)
            radio = []
            for v in tVentas:
                radio.append("{0:.1f}".format((v * 100) / sTotal))
            x = range(0, 10)
            y = json.dumps(xTotal)
            context = {
                    'producto': datos,
                    'color': color,
                    'x': x,
                    'y': y,
                    'i': 10,
                        # 'promfact': '{0:.2f}'.format(pmFact / nFact),
                    'etiketas': json.dumps(label),
                    'radio': json.dumps(radio),
                    'volumen': json.dumps(tVentas)
                   }
                # print(xdata)



    else:
        context = {'val': 0}
    return render(request, 'Graficas/Top_Ten_Segmenta.html', context)

# CodigoOrdenCompra = models.CharField(blank=True, max_length=50)  # Listado/Codigo
    # facturaFk = models.ForeignKey(Facturas, null= True, blank= True, on_delete = models.CASCADE)
    # CodigoProducto = models.CharField(max_length=50)
    # productoFk = models.ForeignKey(Producto, null= True, blank= True, on_delete = models.CASCADE)
    # PrecioNeto = models.FloatField(default=0)
    # Cantidad = models.FloatField(default=0)


def grafica_producto_cliente(request,pk):
    # detalle = Detalle.objects.all().order_by('DescripcionProducto')
    op = 2
    cProduct = request.GET.get('codigo')
    precios = Detalle.objects.filter(CodigoProducto=cProduct).order_by('PrecioNeto')
    if precios:
        xDes_Prod = precios[0].productoFk.DescripcionProducto
        xFormato = precios[0].productoFk.formatoFk.UnidadesFormato
        tTotalFacturas = 0
        xClientes = {}
        for item in precios:
            if item.productoFk.Activo:
                if not item.facturaFk.compradorFk in xClientes:
                    xClientes.update({item.facturaFk.compradorFk:[]})
                    xClientes[item.facturaFk.compradorFk].append(item.facturaFk)
                    tTotalFacturas += 1
                else:
                    if not item.facturaFk in xClientes[item.facturaFk.compradorFk]:
                        xClientes[item.facturaFk.compradorFk].append(item.facturaFk)
                        tTotalFacturas += 1
        cl = []
        pr = []
        vol = []
        dem = []
        objCom = []
        sTotal = 0
        color = []
        for fi,ki in enumerate(xClientes):
            for fa in xClientes[ki]:
                for e in precios:
                    if e.facturaFk == fa:
                        xAdd = False
                        xPos = -1
                        xBas = [pos for pos,i in enumerate(cl) if i == ki.id]
                        for xx in xBas:
                            if "{0:.1f}".format(e.PrecioNeto) == pr[xx]:
                                xAdd = True
                                xPos = xx
                        if not xAdd:
                            cl.append(ki.id)
                            objCom.append(ki)
                            pr.append("{0:.1f}".format(e.PrecioNeto))
                            vol.append(e.PrecioNeto*e.Cantidad)
                            dem.append(e.Cantidad)
                            r = lambda: random.randint(0, 255)
                            color.append('#%02X%02X%02X' % (r(), r(), r()))
                        else:
                            vol[xPos] += e.PrecioNeto*e.Cantidad
                            dem[xPos] += e.Cantidad
                        sTotal += e.PrecioNeto*e.Cantidad


        if cl:
            pr, vol, dem, objCom, cl = zip(*sorted(zip(pr,vol,dem,objCom,cl)))
            # print (pr)
            # print(cl)
            g = zip(objCom,cl,pr,vol,dem)
            data = []
            for a,b,c,d,e in g:
                regF = Facturas()
                regF.compradorFk = a
                regF.TotalDescuentos = c # precio
                regF.TotalFactura = e # demanda o cantidad
                data.append(regF)

            del precios

            i = len(color)
            precios = json.dumps(pr)
            demanda = json.dumps(dem)
            pmFact = 0
            radio = []
            for v in vol:
                radio.append("{0:.1f}".format((v * 100) / sTotal))
            pmFact = "{0:.2f}".format(sTotal/tTotalFacturas)
            color = json.dumps(color)
            context = {
                'producto': xDes_Prod,
                'precios': precios,
                'demandas': demanda,
                'volumen': json.dumps(vol),
                'clientes' : json.dumps(cl),
                'color': color,
                'datos': data,
                'i': i,
                'ventas': sTotal,  # tVentas = [ 125 , 256 , 3568 ]
                'formato': xFormato,
                'promfact': pmFact,
                'tfact': tTotalFacturas,
                'radio': json.dumps(radio),
                'op' : op
            }
    else:
        context = {}
    return render(request, 'Graficas/Graphic_client.html', context)

def lista_base_load(request):
    onclick = request.GET.get('archivo')
    crear_archivo = False#request.GET.get('crearlista')
    context = {'message':'', 'nRegDif' : 0, 'nRegRep' : 0}
    if onclick:
        si = False
        xArchivo = onclick.strip()
        listabase = ''
        try:
            listabase = open(xArchivo, "r")
            tt = listabase.read()
            listabase.close()
            si = True
        except IOError:
            context = {'message': 'El Archivo: ['+onclick+'] no Existe!!! Verifique [<path>]<nom-archivo>', 'nRegDif' : 0, 'nRegRep' : 0}
        if si:
            with codecs.open(xArchivo, "r",encoding='utf-8', errors='ignore') as archivo:
                lines = archivo.readlines()
            tNew = 0
            tDuplica = 0
            for l in lines:
                # print('\n')
                campos = l.split(';')
                if not ListaBase.objects.filter(DescripcionSC = campos[0].strip()):
                    if len(campos) == 4:
                        RegLista = ListaBase()
                        xsave = True
                        for j,c in enumerate(campos):
                            if len(c.strip()) >= 1:
                                if j == 0:
                                    RegLista.DescripcionSC = c.strip()
                                elif j == 1:
                                    RegLista.UnidadesSC = c.strip()
                                elif j == 2:
                                    RegLista.DescripcionC = c.strip()
                                else:
                                    RegLista.UnidadesC = c.strip()
                            else:
                                xsave = False
                        if xsave:
                            tNew += 1
                            RegLista.estado = True
                            RegLista.save()
                        else:
                            # if RegLista is None:
                            del RegLista
                            # else:
                            #     RegLista.delete()
                else:
                    tDuplica += 1
            context = {'message': 'exito','nRegDif': tNew, 'nRegRep': tDuplica}
    else:
        if crear_archivo:
            print('\nEn creacion de lista base\n')
            parse_lista_base()
    return render(request, 'Producto/cargar_lista.html', context)

def lista_base_view(request): #filter(DescripcionSC = campos[0].strip())
    listabase = ListaBase.objects.filter().order_by('DescripcionC')
    if not listabase:
        contexto = {}
    else:
        contexto = {'productos': listabase}
    return render(request, 'Producto/ver_lista_base.html', contexto)


def lista_base_update(request):
    xdatos = ListaBase.objects.filter(estado = False).order_by('DescripcionSC')
    xLen = len(xdatos)
    datos = []
    i = 0
    if xdatos:
        for reg in xdatos:
            datos.append(reg)
            i += 1
            if i>= 20:
                break
    # print(datos)
    context = {'lista': datos,'total' : xLen,'son':i}
    # print('\nEn lista Base')
    if request.GET:
        # productos = Producto.objects.filter(Activo = False)
        # xFile = 'Debug_lBase_Update'+str(len(ListaBase.objects.filter(estado = False)))+'.txt'
        # try:
        #     fout = codecs.open('DebugListUpdate.txt', "w", encoding='utf-8', errors='ignore')
            # fout.close()
        # except IOError:
        #     fout = codecs.open('DebugListUpdate.txt', "w", encoding='utf-8', errors='ignore')
        i = 0
        for val in request.GET: # Para leer los input de la plantilla
            tipo = val.split('-')
            # fout.writelines('\nProcesando: %s\n' % val)
            # print(tipo)
            if str(tipo[0]) == 'D':
                xUni = 'U-' + (tipo[1]).strip()
                xId = int((tipo[1]).strip())
                xListaBase = ListaBase.objects.filter( id = xId)
                # fout.writelines('\nProcesando: NewDesc = %s   NewUnidad = %s' % (request.GET[val],request.GET[xUni]))
                delRegListaBase = False
                if xListaBase:
                    xxxUni = request.GET[xUni] #).strip()
                    # print ('\nVal: = %s' % xxxUni)
                    xUni = (str(xxxUni)).strip()
                    if xUni != '*':
                        delRegListaBase = True
                        xListaBase = xListaBase[0]
                        newVal = (request.GET[val]).strip()
                        oldVal = xListaBase.DescripcionSC
                        # xUni = (request.GET[xUni]).strip()
                        # xListaBase.DescripcionC = val
                        # xListaBase.UnidadesC = xUni
                        xFormato = Formato.objects.filter(DescripcionFormato = xUni)
                        if not xFormato:
                            # fout.writelines('\nCreado Formato: NewUnidad = %s' % xUni)
                            xFormato = Formato()
                            xFormato.CodigoFormato = 'F-'+'{}'.format(len(Formato.objects.all()))
                            xFormato.DescripcionFormato = xUni
                            xFormato.UnidadesFormato = xUni
                            xFormato.save()
                        else:
                            # fout.writelines('\nFormato ya Existe = %s' % xUni)
                            xFormato = xFormato[0]
                        productoCs = Producto.objects.filter( DescripcionProducto = newVal , Activo=True)
                        if productoCs:
                            newProd = productoCs[0]
                            # fout.writelines('\nProducto ya calisficado = %s' % newProd.DescripcionProducto)
                            productoCs = Producto.objects.filter(DescripcionProducto=oldVal, Activo=False)
                            for regProd in productoCs:
                                regDetalles = Detalle.objects.filter(productoFk=regProd)
                                for regD in regDetalles:
                                    regD.productoFk = newProd
                                    regD.CodigoProducto = newProd.CodigoProducto
                                    regD.save()
                                # fout.writelines('\nProducto %s Eliminado (Arch Productos)' % regProd.DescripcionProducto)
                                regProd.delete()
                        else:
                            productoCs = Producto.objects.filter(DescripcionProducto = oldVal, Activo=False)
                            newProd = None
                            sw = True
                            xDelete = False
                            for regProd in productoCs:
                                if sw:
                                    regProd.DescripcionProducto = newVal
                                    regProd.formatoFk = xFormato
                                    regProd.listaBasepk = xListaBase
                                    delRegListaBase = False
                                    regProd.Activo = True
                                    regProd.save()
                                    sw = False
                                    newProd = regProd
                                    # fout.writelines('\nProducto %s Claisficado (Arch Productos)' % regProd.DescripcionProducto)
                                else:
                                    xDelete = True
                                regDetalles = Detalle.objects.filter(productoFk = regProd)
                                for regD in regDetalles:
                                    regD.productoFk = newProd
                                    regD.CodigoProducto = newProd.CodigoProducto
                                    regD.save()
                                if xDelete:
                                    # fout.writelines('\nProducto %s Eliminado (Arch Productos)' % regProd.DescripcionProducto)
                                    if not regProd is None:
                                        regProd.delete()

                if delRegListaBase:
                    # fout.writelines('\nProducto %s Eliminado (Lista Base)' % xListaBase.DescripcionC)
                    if not xListaBase is None:
                        xListaBase.delete()
                else:
                    xListaBase.DescripcionC = newVal
                    xListaBase.UnidadesC = xUni
                    xListaBase.estado = True
                    xListaBase.save()
                    # fout.writelines('\nProducto %s Actualizado (Lista Base)' % xListaBase.DescripcionC)
                i += 1
                # fout.writelines('\n')
                # if i>=20:
                #     break
                    #
        # fout.writelines('\n\n\nTotal Productos Procesados %s' % i)
        # fout.close()
                    # print(xUni)
                    # xUni = 'U-'+(tipo[1]).strip()
                    # print('\nDescp =  %s  Uni  %s' % (xListaBase.DescripcionSC,xListaBase.DescripcionC))
            #      t += 1
        #     print('\nDescp =  %s' % val)
        xdatos = ListaBase.objects.filter(estado=False).order_by('DescripcionSC')
        xLen = len(xdatos)
        datos = []
        i = 0
        if xdatos:
            for reg in xdatos:
                datos.append(reg)
                i += 1
                if i >= 20:
                    break
        # print(datos)
        context = {'lista': datos, 'total': xLen, 'son': i}
        # return render(request, 'index.html')
    return render(request, 'Producto/add_list_base.html', context)


def cotejo_producto_lista_base(request):
    xproducto = Producto.objects.filter(Activo = True).order_by('DescripcionProducto')

    context = {'product_list' : xproducto}
    return render(request, 'Producto/product_cotejo.html', context)


def possible_price(request):
    detalle = Detalle.objects.all()
    context = {'val': 0}
    if detalle:
        data = {}
        xtop = {}
        for regDet in detalle:
            if regDet.productoFk.Activo:
                if not regDet.productoFk in data:
                    xtop.update({regDet.productoFk: 0})
                    data.update({regDet.productoFk: []})
                    data[regDet.productoFk].append({'nprecios': 0, 'precio': [], 'cant': [], 'ventas' : []})
                for k in data[regDet.productoFk]:
                    if not regDet.PrecioNeto in k['precio']:
                        k['precio'].append(regDet.PrecioNeto)
                        k['cant'].append(regDet.Cantidad)
                        k['nprecios'] += 1
                        xtop[regDet.productoFk] += 1
                    else:
                        k['cant'][[j for j, val in enumerate(k['precio']) if val == regDet.PrecioNeto][
                            0]] += regDet.Cantidad
        for k in data:
            for p in data[k]:
                if p['nprecios'] > 0:
                    for col in range(0,p['nprecios']):
                        p['ventas'].append(p['cant'][col] * p['precio'][col])
                    pMax = max(p['precio']) #p['precio'][(p['ventas']).index(max(p['ventas']))]
                    pMin = p['precio'][(p['ventas']).index(min(p['ventas']))]
                    if pMin <= 0:
                        pMin = pMax
                    if pMax == pMin:
                        pMax = pMin+pMin*0.03
                        pMed = (pMax + pMin)/2
                    else:
                        if pMax > pMin:
                            pMed = sum(p['precio'])/len(p['precio'])
                        else:
                            pMax = sum(p['precio']) / len(p['precio'])
                            pMed = (pMax + pMin) / 2
                    k.PrecioMin = truncate(pMin,2)
                    k.PrecioMedio = truncate(pMed,2)
                    k.PrecioMax = truncate(pMax,2)
                    k.PrecioProm = truncate(sum(p['precio']) / len(p['precio']),2)
                    k.save()
        context = {'product_list' : Producto.objects.filter(Activo = True).order_by('DescripcionProducto') }
    return render(request, 'Producto/precio_sugerido.html', context)


def list_price_sugg(request):
    xproducto = Producto.objects.filter(Activo = True).order_by('DescripcionProducto')
    context = {'product_list' : xproducto}
    return render(request, 'Producto/precio_sugerido.html', context)

def Codigos_Proveedores(request):
    file_in = 'files/codigoprov.txt'
    codigos = []
    des_prov = []
    t = 0
    xDescProv = Proveedor.objects.all()
    for reg in xDescProv:
        codigos.append(reg.CodigoProveedor)
        des_prov.append(reg.NombreProveedor)
    if check_file(file_in, 'r'):
        with codecs.open(file_in, 'r', encoding='utf-8', errors='ignore') as archivo:
            lines = archivo.readlines()
        for linea in lines:
            yCod = str(linea.strip(' \n\r\t'))
            if not yCod in codigos:
                t += 1
                codigos.append(yCod)
                des_prov.append('Desconocido')
    for i in range(6):
        codigos.append('')
        des_prov.append('Nuevo')
    context = {'lista_c': codigos, 'lista_d': des_prov}
    if request.GET:
        xcodigos = []
        with codecs.open(file_in, 'w', encoding='utf-8', errors='ignore') as archivo:
            for val in request.GET:
                print('\nVariable: %s' % val)
                xCod = (request.GET[val]).strip()
                print('\nCodigo leido: %s' % xCod)
                if (len(xCod)>1)and(not xCod in xcodigos):
                    xcodigos.append(xCod)
                    archivo.writelines('%s\n' % xCod)
        context = {}
    return render(request, 'Proveedor/add_cod_provee.html', context)


#ProveedorListView
class proveedorListView(generic.ListView):
    model = Proveedor


    def get_queryset(self):
        return Proveedor.objects.all().order_by('NombreProveedor')

class ProveedorDetailView(generic.DetailView):
    model = Proveedor

class CompradorListView(generic.ListView):
    model = Comprador

    def get_queryset(self):
        return Comprador.objects.all().order_by('NombreOrganismoComprador')

class CompradorDetailView(generic.DetailView):
    model = Comprador

class ProductoListView(generic.ListView):
    # p = Producto.objects.all().order_by('DescripcionProducto')
    model = Producto #.objects.all().

    def get_queryset(self):
        return Producto.objects.filter(Activo = True).order_by('DescripcionProducto')

class ProductoSCListView(generic.ListView):
    # p = Producto.objects.all().order_by('DescripcionProducto')
    model = Producto #.objects.all().

    def get_queryset(self):
        return Producto.objects.filter(Activo = False).order_by('DescripcionProducto')

class ProductoDetailView(generic.DetailView):
    model = Producto

class RegionListView(generic.ListView):
    model = Region

class CategoriaListView(generic.ListView):
    model = Categoria


# 130625
# 62363
# 1504575
# 66345
# 105161
# 1398522
# 1365205
# 107665
# 122683
# 1581717
# 1318873
# 1388986
# 1042379
# 21173
# 1282195
# 1393163
# 194499
# 243082
# 1296964
# 71227
# 96381
# 1167949
# 287422
# 1578774
# 1547171
# 1394299
# 1583741
#

# Download backup Heroku
#
# heroku pg:backups:capture
# heroku pg:backups:download
# Retaurar DB
# pg_restore --verbose --clean --no-acl --no-owner -h localhost -U foodsys -d foodsys latest.dump
#
# Create dump file
# PGPASSWORD=mypassword pg_dump -Fc --no-acl --no-owner -h localhost -U myuser mydb > mydb.dump
