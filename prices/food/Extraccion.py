import codecs
import requests
import math
import time
import json
from datetime import datetime,timedelta
from Modelos.Producto.models import Producto
from Modelos.Facturas.models import Facturas
from Modelos.Detalle.models import *
from  Modelos.Categoria.models import *
from  Modelos.Region.models import *
from Modelos.Formato.models import *
from Modelos.ListaBase.models import *
from Modelos.Proveedor.models import *
from Modelos.Comprador.models import *
from food.machine_learning import *


def check_file(name_file,open_mode):
    noError = True
    try:
        file = open(name_file, open_mode)
        if open_mode in ['r','r+','a','a+']:
            tt = file.read()
        else:
            file.writelines(' ')
        file.close()
    except IOError:
        noError = False
        print('\nError de apertura del archivo:  %s' % name_file)
    return noError


def clasifica(xDesSC):
    Prod = ListaBase.objects.filter(DescripcionSC = xDesSC, estado = True)
    if Prod:
        return Prod[0].DescripcionC,Prod[0].UnidadesC, Prod[0]
    else:
        return '=','',None

def getCatego(xDesc):
    xCat = xDesc.split('/')
    xCad = 'Alimentos, bebidas y tabaco'
    xSw = False
    xxCad = xCat[0].strip(' \n\r')
    if len(xCat) >= 1:
        xxCad = xCat[0].strip(' \n\r')
        xxCad = xxCad.lower()
        if xxCad == xCad.lower():
            xSw = True
            # yCat = xCat[0]
    return xSw

def Scrapy(url,tiempo):
     if tiempo[0]>1:
        r = requests.get(url,timeout=(tiempo[0], tiempo[1]))
     else:
         r = requests.get(url)
     if (r.status_code == requests.codes.ok):
         return r.json(),-1
     else:
         return [],r.status_code


def std_producto(xDesSC):
    l = a = linin = xDesSC.strip()
    linea = ''
    if l.find('-') > 2:
        a = l[l.find('-'):len(l)]
        a = a.lstrip(' -')
        a.rstrip(' ')
        pedazos = a.split()
    else:
        linin = linin.strip()
        pedazos = linin.split()
    if pedazos:
        # print('\nPedazos: %s ' % pedazos)
        xUni = ''
        tam = len(pedazos)
        if pedazos[tam - 1].isdigit():
            tam = len(pedazos) - 1
            if pedazos[tam - 1] in ['k', 'K', 'kg', 'KG', 'KILO', 'KILOS','KILOGRAMO','KILOGRAMOS']:
                pedazos[tam - 1] = 'KG'
                xUni = 'KG'
            elif pedazos[tam - 1] in ['unidad', 'UNIDAD', 'unidades', 'UNIDADES']:
                pedazos[tam - 1] = 'UN'
                xUni = 'UN'
            elif pedazos[tam - 1] in ['PAQ.', 'PAQ', 'PAQUETE', 'paquete']:
                pedazos[tam - 1] = 'PAQUETE'
                xUni = 'PAQUETE'
            elif pedazos[tam - 1] in ['ATADO', 'atado', 'AT', 'at']:
                pedazos[tam - 1] = 'ATADO'
                xUni = 'ATADO'
        for pos, p in enumerate(pedazos):
            if p in ['MALLA', 'CAJA']:
                xUni = p
            if p in ['KILOS', 'KILOGRAMOS','KL','KLG','KILO']:
                pedazos[pos] = 'KG'
                xUni = 'KG'
            if p in ['1', 'DE', 'APROX','APROXIMADO','APROXIMADA','APROX.']:
                pedazos[pos] = ''
        for i in range(0, tam):
            if len(pedazos[i]) > 1:
                xx = pedazos[i]
                linea += xx.strip()
                if i != tam - 1:
                    linea += ' '
        # linea = linea.replace('1','')
        # linin = linin.strip('\n')
        # print('\nDes Entra = %s Des = %s  Uni = %s ' % (linin,linea,xUni))
        yProd = Producto.objects.filter(DescripcionProducto = linea )
        if yProd:
            linea = yProd[0].DescripcionProducto
            if not yProd[0].formatoFk is None:
               xUni = yProd[0].formatoFk.UnidadesFormato
        return linea, xUni
    else:
        return 'Nothing', 'Desc'
        # fout.writelines('%s;xxxx;%s;%s\n' % (linin, linea, xUni))
            # print(linea)

def truncate(number, digits):
    stepper = pow(10.0, digits)
    return math.trunc(stepper * number) / stepper

def parse_lista_base():
    fout = codecs.open("files/listabase.txt","w",encoding='utf-8', errors='ignore')
    if not fout:
        print('\n\nError de Apertura\n\n')
        fout.close()
    else:
        lista = ListaBase.objects.filter(estado = True)
        for regLista in lista:
            fout.writelines('%s;%s;%s;%s\n' % (regLista.DescripcionSC ,  regLista.UnidadesSC,  regLista.DescripcionC,  regLista.UnidadesC))
        fout.close()

def extrac_proveedores_automatic():
    file_in = '../files/codigoprov.txt'
    codigos = []
    si = check_file(file_in, 'r')
    if si:
        with codecs.open(file_in, 'r', encoding='utf-8', errors='ignore') as archivo:
            lines = archivo.readlines()
        for linea in lines:
            yCod = str(linea.strip(' \n\r\t'))
            if not yCod in codigos:
                codigos.append(yCod)
        print('\nCidgos: %s' % codigos)
        fechaI = datetime.strptime(time.strftime('%Y-%m-%d'),'%Y-%m-%d')
        fechaF = datetime.strptime(time.strftime('%Y-%m-%d'),'%Y-%m-%d')
        # fechaI = datetime.strptime(fechaI, '%Y-%m-%d')
        print('\nFecha actual:  %s' % fechaF)
        if (fechaI <= fechaF) and (len(codigos)):
            xurlbase = 'http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?'
            ticket = '&ticket=C74E17AB-BB1C-4CD2-9495-C55A5D2A2E56'
            rangodias = []
            undia = timedelta(days=1)
            while fechaI <= fechaF:
                if datetime.weekday(fechaI) in [0, 1, 2, 3, 4]:
                    rangodias.append(datetime.strftime(fechaI, '%Y-%m-%d'))
                fechaI = fechaI + undia
            codprov = codigos  # .rstrip().split('\r\n')
            # codigos = []
            tFact = len(Facturas.objects.filter(EstadoExtraccion=False).order_by('Fecha'))
            i = 0
            tiempo = [-1, 0]
            for xcod in codprov:
                if len(xcod) > 2:
                    for fecha in rangodias:  # datetime.strptime(,'%Y-%m-%d')
                        reg = Facturas.objects.filter(CodigoProveedor=xcod, Fecha=datetime.strptime(fecha, '%Y-%m-%d'))
                        # print('\n %s ' % reg)
                        if not reg:
                            urlbase = xurlbase
                            a, m, d = fecha.split('-')
                            urlbase += 'fecha='
                            urlbase += d + m + a
                            codprov = xcod.replace(" ", "")
                            urlbase += '&CodigoProveedor=' + codprov + ticket
                            # return render(request, 'extraccionfacturas/mensajes.html')
                            print('\nURL Orden Extract: %s' % urlbase)
                            Json, code = Scrapy(urlbase, tiempo)
                            # print('\nCode = %s' % code)
                            # print('\nJson = %s ' % Json)

                            if Json:
                                for Data in Json['Listado']:
                                    # codigos.append(Data['Codigo'])

                                    if len(Facturas.objects.filter(CodigoOrdenCompra=Data['Codigo'])) <= 0:
                                        i += 1
                                        orden = Facturas(CodigoOrdenCompra=Data['Codigo'], CodigoProveedor='%s' % xcod,
                                                         Fecha=datetime.strptime(fecha, '%Y-%m-%d'), TotalFactura=0,
                                                         EstadoExtraccion=False)
                                        orden.save()

def extraer_facturas_automatic():
    xurlbase = 'http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?'
    ticket = '&ticket=C74E17AB-BB1C-4CD2-9495-C55A5D2A2E56'
    urlbase = 'http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?'
    urlbase += 'codigo='
    tTotalFacturas = 0
    dib = ['|', '/', '-', '|', '\\']
    tiempo = [-1, 0]
    xIntentos = True
    if len(ListaBase.objects.all()) > 1:  # .order_by('Fecha')
        Nerror = 0
        while xIntentos:
            facturas = Facturas.objects.filter(EstadoExtraccion=False).order_by('Fecha')  # Solo las facturas que aun no se han extraido
            # xIntentos = xIntentos + 1
            cExtract = 0
            kInd = -1
            if facturas:
                for orden in facturas:
                    url = urlbase + orden.CodigoOrdenCompra.replace("\n", "") + ticket
                    xFecha = orden.Fecha
                    xOrdenCompra = orden.CodigoOrdenCompra
                    if Nerror >= 10:  # tiempo = [-1,0]  connect and read
                        if Nerror % 10:
                            tiempo[0] += 5
                            tiempo[1] = 10
                        Json, code = Scrapy(url, tiempo)
                    else:
                        Json, code = Scrapy(url, tiempo)
                    if code != -1:
                        Nerror += 1
                        # print('\nError code = %s  Nerrores %s' % (code,Nerror))
                    print('\nURL Orden Client Extract: %s' % url)
                    # print('\nJson = %s ' % Json)
                    if Json:
                        # kInd = kInd + 1
                        # if kInd >= len(dib):
                        #     kInd = 0
                        # mensa = 'Proccess {}'.format(dib[kInd])
                        # messages.success(request, mensa)
                        cExtract = cExtract + 1
                        regReg = Region.objects.filter(
                            DescripcionRegion=(Json['Listado'][0]['Comprador']['RegionUnidad']).replace("\n",
                                                                                                        ""))  # Region
                        if not regReg:
                            region = Region()
                            region.DescripcionRegion = (Json['Listado'][0]['Comprador']['RegionUnidad']).replace("\n",
                                                                                                                 "")
                            region.CodigoRegion = '{}'.format(len(Region.objects.all()))
                            region.save()
                            idregion = region.id
                        else:
                            idregion = regReg[0].id
                        regFact = Facturas.objects.filter(CodigoOrdenCompra=orden.CodigoOrdenCompra)  # Facturas
                        if regFact:
                            region = ''
                            regFactura = regFact[0]
                            regFactura.CodigoOrganismoComprador = Json['Listado'][0]['Comprador']['CodigoOrganismo']
                            regFactura.CodigoRegion = '{}'.format(
                                idregion)  # Json['Listado'][0]['Comprador']['RegionUnidad']#RegionUnidad
                            regFactura.TotalFactura = Json['Listado'][0]['TotalNeto']
                            regFactura.EstadoExtraccion = True
                            xSave = False  # Solo Comprador
                            regComprador = Comprador.objects.filter(
                                CodigoOrganismoComprador=Json['Listado'][0]['Comprador'][
                                    'CodigoOrganismo'])  # Comprador
                            if not regComprador:
                                regComprador = Comprador()
                                regComprador.CodigoOrganismoComprador = Json['Listado'][0]['Comprador'][
                                    'CodigoOrganismo']
                                regComprador.NombreOrganismoComprador = Json['Listado'][0]['Comprador'][
                                    'NombreOrganismo']
                                regComprador.RutUnidadComprador = Json['Listado'][0]['Comprador']['RutUnidad']
                                regComprador.ActividadComprador = Json['Listado'][0]['Comprador']['Actividad']
                                regComprador.DireccionUnidadComprador = Json['Listado'][0]['Comprador'][
                                    'DireccionUnidad']
                                regComprador.NombreContactoComprador = Json['Listado'][0]['Comprador']['NombreContacto']
                                regComprador.CargoContactoComprador = Json['Listado'][0]['Comprador']['CargoContacto']
                                if len(Json['Listado'][0]['Comprador']['FonoContacto'][:79]) > 80:
                                    xContact = Json['Listado'][0]['Comprador']['FonoContacto'][:79]
                                else:
                                    xContact = Json['Listado'][0]['Comprador']['FonoContacto']
                                regComprador.FonoContactoComprador = xContact
                                regComprador.MailContactoComprador = Json['Listado'][0]['Comprador']['MailContacto']
                                xSave = True
                            else:
                                regComprador = regComprador[0]
                            regReg = Region.objects.filter(
                                DescripcionRegion=(Json['Listado'][0]['Proveedor']['Region']).replace("\n", ""))
                            if not regReg:
                                region = Region()
                                region.DescripcionRegion = (Json['Listado'][0]['Proveedor']['Region']).replace("\n", "")
                                region.CodigoRegion = '{}'.format(len(Region.objects.all()))
                                region.save()
                            else:
                                region = regReg[0]
                            regProveedor = Proveedor.objects.filter(
                                CodigoProveedor=Json['Listado'][0]['Proveedor']['Codigo'])  # Proveedor
                            if not regProveedor:
                                regProveedor = Proveedor()
                                regProveedor.CodigoProveedor = Json['Listado'][0]['Proveedor']['Codigo']
                                regProveedor.NombreProveedor = Json['Listado'][0]['Proveedor']['Nombre']
                                regProveedor.ActividadProveedor = Json['Listado'][0]['Proveedor']['Actividad']
                                regProveedor.RutSucursalProveedor = Json['Listado'][0]['Proveedor']['RutSucursal']
                                regProveedor.DireccionProveedor = Json['Listado'][0]['Proveedor']['Direccion']
                                regProveedor.NombreContactoProveedor = Json['Listado'][0]['Proveedor']['NombreContacto']
                                regProveedor.CargoContactoProveedor = Json['Listado'][0]['Proveedor']['CargoContacto']
                                regProveedor.FonoContactoProveedor = Json['Listado'][0]['Proveedor']['FonoContacto'][
                                                                     :79]
                                regProveedor.MailContactoProveedor = Json['Listado'][0]['Proveedor']['MailContacto']
                                regProveedor.regionPk = region
                                regProveedor.save()
                            else:
                                regProveedor = regProveedor[0]
                            if xSave:  # Solo Comprador
                                regComprador.regionPk = region
                                regComprador.save()
                            # Actualizar Campo de llave foranea de Facturas
                            regFactura.compradorFk = regComprador
                            regFactura.proveedorFk = regProveedor
                            regFactura.regionFk = region
                            xSaveFactura = False  # Si hay registros en detalle Guardar Factura
                            # regFactura.save() # Almacena en DB las Ordenes de Compra  Pero aqui no debe ir
                            for item in Json['Listado'][0]['Items']['Listado']:
                                cont = getCatego((item['Categoria']).strip())
                                # xDesProd = item['EspecificacionComprador']
                                if cont:  # xDesProd.find('PARA')<=0:
                                    xSaveFactura = True  # Hay al menos un registro
                                    xDesProd = item['EspecificacionComprador']
                                    if (item['EspecificacionComprador']).find('(') > -1:
                                        if (item['EspecificacionComprador']).find('(') < 5:
                                            if (item['EspecificacionComprador']).find(')') > -1:
                                                xDesProd = (item['EspecificacionComprador']).split(')')[1]
                                    xDesProd = xDesProd.strip(',.:;-+= ')
                                    if xDesProd.find('RM') > 0:
                                        xDesProd = xDesProd[0:xDesProd.find('RM')]
                                    if xDesProd.find('REGION') > 0:
                                        xDesProd = xDesProd[0:(xDesProd.find('REGION') - 2)]
                                    xxDesProd = xDesProd.rstrip(',.:;-+= V IV')
                                    if len(xDesProd) > 80:
                                        xxDesProd = xDesProd[0:79] # Verifica si el producto ya esta clasificado
                                    xDesProd, xUni, xListaBase = clasifica(xxDesProd)
                                    xClasificado = False
                                    if xDesProd != '=':
                                        xClasificado = True
                                    else:
                                        xxDesProd = xxDesProd.strip(' \n\t\r')
                                        if not xxDesProd in ListaBase.objects.filter(DescripcionSC=xxDesProd,estado=False):
                                            regListaBase = ListaBase()
                                            regListaBase.estado = False
                                            regListaBase.DescripcionSC = xxDesProd
                                            # Standariza descripcion del producto por Apredizaje automatico
                                            regListaBase.DescripcionC, regListaBase.UnidadesC, kSw = Clasificar(xxDesProd)
                                            if kSw:
                                                yxListaBase = ListaBase.objects.filter(DescripcionC=regListaBase.DescripcionC)  # estado=True)
                                                if yxListaBase:
                                                    regListaBase.productopk = yxListaBase[0]
                                                else:
                                                    regListaBase.productopk = None
                                                regListaBase.save()
                                                xListaBase = regListaBase
                                            else:
                                                # Standariza descripcion del producto por semi-clasificador
                                                regListaBase.DescripcionC, regListaBase.UnidadesC = std_producto(xxDesProd)
                                                yxListaBase = ListaBase.objects.filter(DescripcionC=regListaBase.DescripcionC) #  estado=True)
                                                if yxListaBase:
                                                    regListaBase.productopk = yxListaBase[0]
                                                else:
                                                    regListaBase.productopk = None
                                                regListaBase.save()
                                                xListaBase = regListaBase
                                        else:
                                            yxListaBase = ListaBase.objects.filter(DescripcionSC=xxDesProd, estado=True)
                                            if not yxListaBase:
                                                yxListaBase = ListaBase.objects.filter(DescripcionSC=xxDesProd,
                                                                                       estado=False)
                                            if yxListaBase:
                                                xListaBase = yxListaBase[0]
                                            else:
                                                xListaBase = None
                                        xDesProd = xxDesProd

                                    regProducto = Producto.objects.filter(DescripcionProducto=xDesProd)
                                    if not regProducto:
                                        regProducto = Producto()
                                        # regProducto.CodigoCategoria = str(item['CodigoCategoria']).strip() #item['CodigoCategoria']
                                        cad = xDesProd.split()
                                        if not xClasificado:
                                            if len(cad) > 0:
                                                if cad[0].find(')') > -1:
                                                    xcad = cad[0].split(')')
                                                    if len(xcad) > 0:
                                                        cad = xcad[1]
                                                    else:
                                                        cad = '0000'
                                                else:
                                                    cad = cad[0]
                                            else:
                                                cad = '00002'
                                        else:
                                            cad = cad[0]

                                        xCodProducto = regProducto.CodigoProducto = cad + '-' + '{}'.format(
                                            len(Producto.objects.all()))
                                        regProducto.DescripcionProducto = xDesProd  # yDes
                                        if xClasificado:
                                            Uni = xUni
                                            # u = xUni.split()
                                            xCodFormato = 'F-'
                                        else:
                                            Uni = 'Formato Sin Clasificar'
                                        regFormato = Formato.objects.filter(DescripcionFormato=Uni)
                                        if not regFormato:
                                            if xClasificado:
                                                regFormato = Formato()
                                                regFormato.CodigoFormato = xCodFormato + '{}'.format(
                                                    len(Formato.objects.all()))
                                                regFormato.DescripcionFormato = Uni
                                                regFormato.UnidadesFormato = Uni
                                                regFormato.save()
                                            else:
                                                # regFormato.DescripcionFormato = Uni
                                                regFormato = None
                                        else:
                                            regFormato = regFormato[0]
                                        regProducto.formatoFk = regFormato
                                        regProducto.PrecioCosto = 0;
                                        regProducto.PrecioMin = 0;
                                        regProducto.PrecioMedio = 0
                                        regProducto.PrecioMax = item['PrecioNeto']
                                        regProducto.FechaModifica = xFecha
                                        regProducto.Activo = True
                                        xCategoria = 'C-D'
                                        if str(item['CodigoCategoria']).strip():
                                            xCategoria = str(item['CodigoCategoria']).strip()
                                        regCategoria = Categoria.objects.filter(CodigoCategoria=xCategoria)
                                        if not regCategoria:
                                            regCategoria = Categoria()
                                            regCategoria.CodigoCategoria = xCategoria
                                            if (item['Categoria']).strip():
                                                regCategoria.DescripcionCategoria = (item['Categoria']).strip()[:79]
                                            else:
                                                regCategoria.DescripcionCategoria = 'Categoria Desconocida'
                                            regCategoria.save()
                                        else:
                                            regCategoria = regCategoria[0]
                                        regProducto.CodigoCategoria = xCategoria
                                        regProducto.categoriaFk = regCategoria
                                        regProducto.listaBasepk = xListaBase
                                        if not xClasificado:
                                            regProducto.Activo = False
                                        regProducto.save()
                                    else:
                                        xCodProducto = regProducto[0].CodigoProducto
                                        regProducto = regProducto[0]
                                    regDetalle = Detalle()
                                    regDetalle.CodigoOrdenCompra = xOrdenCompra
                                    regDetalle.CodigoProducto = xCodProducto
                                    regDetalle.PrecioNeto = item['PrecioNeto']
                                    regDetalle.Cantidad = item['Cantidad']
                                    regDetalle.productoFk = regProducto
                                    # regFactura.save()
                                    regDetalle.facturaFk = regFactura
                                    regDetalle.save()
                            if xSaveFactura:
                                regFactura.save()
                            else:
                                if regFactura:
                                    regFactura.delete()
            if len(Facturas.objects.filter(EstadoExtraccion=False)) >= 1:
                xIntentos = True
                if Nerror >= 100:
                    xIntentos = False
            else:
                xIntentos = False

def automatic_price():
    detalle = Detalle.objects.all()
    if detalle:
        fout = codecs.open("../files/precios.txt", "w", encoding='utf-8', errors='ignore')
        if not fout:
            print('\n\nError de Apertura\n\n')
            fout.close()
        data = {}
        xtop = {}
        for regDet in detalle:
            if regDet.productoFk.Activo:
                if not regDet.productoFk in data:
                    xtop.update({regDet.productoFk: 0})
                    data.update({regDet.productoFk: []})
                    data[regDet.productoFk].append({'nprecios': 0, 'precio': [], 'cant': [], 'ventas' : []})
                for k in data[regDet.productoFk]:
                    if not regDet.PrecioNeto in k['precio']:
                        k['precio'].append(regDet.PrecioNeto)
                        k['cant'].append(regDet.Cantidad)
                        k['nprecios'] += 1
                        xtop[regDet.productoFk] += 1
                    else:
                        k['cant'][[j for j, val in enumerate(k['precio']) if val == regDet.PrecioNeto][
                            0]] += regDet.Cantidad
        for k in data:
            for p in data[k]:
                if p['nprecios'] > 0:
                    for col in range(0,p['nprecios']):
                        p['ventas'].append(p['cant'][col] * p['precio'][col])
                    pMax = max(p['precio']) #p['precio'][(p['ventas']).index(max(p['ventas']))]
                    pMin = p['precio'][(p['ventas']).index(min(p['ventas']))]
                    if pMin <= 0:
                        pMin = pMax
                    if pMax == pMin:
                        pMax = pMin+pMin*0.03
                        pMed = (pMax + pMin)/2
                    else:
                        if pMax > pMin:
                            pMed = sum(p['precio'])/len(p['precio'])
                        else:
                            pMax = sum(p['precio']) / len(p['precio'])
                            pMed = (pMax + pMin) / 2
                    k.PrecioMin = truncate(pMin,2)
                    k.PrecioMedio = truncate(pMed,2)
                    k.PrecioMax = truncate(pMax,2)
                    k.PrecioProm = truncate(sum(p['precio']) / len(p['precio']),2)
                    k.save()
                    if fout:
                        fout.writelines('%s;%s;%s;%s;%s\n' % (k.DescripcionProducto,k.formatoFk.UnidadesFormato,k.PrecioMin,k.PrecioMedio,k.PrecioMax))
        if fout:
            fout.close()