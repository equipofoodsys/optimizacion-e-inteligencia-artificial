from django.conf.urls import url

from . import views

app_name = 'food'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^Proveedores/$', views.proveedorListView.as_view(), name='proveedor'),
    url(r'^Proveedores/(?P<pk>\d+)$', views.ProveedorDetailView.as_view(), name='proveedor-detail'),
    url(r'^Compradores/$', views.CompradorListView.as_view(), name='comprador'),
    url(r'^Comprador/(?P<pk>\d+)$', views.CompradorDetailView.as_view(), name='comprador-detail'),
    url(r'^Producto/$', views.ProductoListView.as_view(), name='producto'),
    url(r'^Producto/SC$', views.ProductoSCListView.as_view(), name='producto_sc'),
    url(r'^Producto/(?P<pk>\d+)$', views.ProductoDetailView.as_view(), name='producto-detail'),
    url(r'^Extraccion/Facturas/Ordenes$', views.request_page, name='persistir_facturas'),
    url(r'^Extraccion/Proveedor/$', views.ExtraerOrdenesCompra, name='extraccion_proveedor'),
    url(r'^Extraccion/Proveedor/Codigos$', views.Codigos_Proveedores, name='lista_proveedor_extract'),
    url(r'^Extraccion/Facturas/$', views.ExtraerFacturas, name='extraccion_facturas'),
    url(r'^Ordenes/Compra/$', views.lista_ordenes, name='listar_ordenes'),
    url(r'^Region/$', views.RegionListView.as_view(), name='region'),
    url(r'^Categorias/$', views.CategoriaListView.as_view(), name='categorias'),
    url(r'^Listar/Ordenes/(?P<orden>\d+)/$', views.detalle_ordenescompra, name='detalle_factura'),
    #
    url(r'^Listar/Ordenes$', views.ListarOrdenesCompraxFecha, name='Listar_Ordenes'),
    url(r'^Productos/Graficos$', views.listaproductografica, name='grafica_producto'),
    url(r'^Productos/Graficos/Clientes/(?P<pk>\d+)$', views.grafica_producto_cliente, name='grafica_producto_cliente'),
    url(r'^Productos/Graficos/(?P<pk>\d+)$', views.productgraphic, name='grafico'),
    url(r'^Productos/Top_10$', views.top_ten, name='top_10'),
    url(r'^Productos/Top_Segmentados$', views.mayor_segmentacion, name='top_10_segmenta'),
    url(r'^Productos/ListaBase$', views.lista_base_load, name='add_lista_base'),
    url(r'^Productos/ListaBase/view$', views.lista_base_view, name='view_lista_base'),
    url(r'^Productos/ListaBase/update$', views.lista_base_update, name='update_lista_base'),
    url(r'^Productos/Productos/cotejo$', views.cotejo_producto_lista_base, name='cotejo_producto_lista_base'),
    url(r'^Productos/Precio_Sugerido$', views.possible_price, name='sugiere_precios'),
    url(r'^Productos/Listar_Sugeridos$', views.list_price_sugg, name='lista_precios_sugeridos'),
    # lista_proveedor_extract
    # url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'), grafica_producto_cliente update_lista_base
    # url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'), add_lista_base
    # url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
]