from django import forms
from django.forms import widgets
from django.forms.widgets import CheckboxSelectMultiple

from  Modelos.Facturas.models import Facturas
from Modelos.Proveedor.models import *


"""
def get_categorized_skills(): 
    skills = {} 
    for s in Skill.objects.values('pk', 'name', 'category__name').order_by('category__name'): 
     if s['category__name'] not in skills.keys(): 
      skills[s['category__name']] = [] 
     skills[s['category__name']].append((s['pk'], s['name'])) 
    return skills 

class SkillSelectionForm(forms.Form): 
    def __init__(self, *args, **kwargs): 
     super(SkillSelectionForm, self).__init__(*args, **kwargs) 
     skills = get_categorized_skills() 
     for idx, cat in enumerate(skills.keys()): 
      field_name = u'category-{0}'.format(idx) 
      display_name = cat 
      self.fields[field_name] = forms.MultipleChoiceField(choices=skills[cat], widget=forms.CheckboxSelectMultiple, label=display_name) 
"""
def get_categorized_skills():
    codProv = {}
    # xNomProv = []
    for s in Proveedor.objects.values('id','CodigoProveedor','NombreProveedor').order_by('CodigoProveedor'):
        if s['CodigoProveedor'] not in codProv.keys():
            codProv[s['CodigoProveedor']]=[]
            # xNomProv.append(s['NombreProveedor'])
        codProv[s['CodigoProveedor']].append((s['CodigoProveedor'], s['CodigoProveedor']+' - '+s['NombreProveedor']))
    return codProv


class ConsultaProveedorForms(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ConsultaProveedorForms, self).__init__(*args, **kwargs)
        skills = get_categorized_skills()
        # self.CodigosProveedor = ''
        for idx, cat in enumerate(skills.keys()):
            field_name = 'codigo-{0}'.format(idx)
            self.fields[field_name] = forms.MultipleChoiceField(required = False , choices= skills[cat],
                                                                widget=forms.CheckboxSelectMultiple, label='')

    FechaInicio = forms.DateField(widget=forms.SelectDateWidget(),label='Fecha Inicio')
    FechaFin = forms.DateField(widget = forms.SelectDateWidget(),label='Fecha Fin')
    CodigosProveedor = forms.CharField(widget=forms.Textarea, required=False, label='Cod. de Proveedores')

class ListarOrdenesExtraidasxFechaForms(forms.Form):
    FechaInicio = forms.DateField(widget=forms.SelectDateWidget(),label='Fecha Inicio')
    FechaFin = forms.DateField(widget = forms.SelectDateWidget(),label='Fecha Fin')

class ConsultaFacturasForms(forms.ModelForm):

     class Meta:
         model=Facturas

         fields = [
             'CodigoOrdenCompra',
             'CodigoProveedor',
              'Fecha',
         ]

         labels = {
             'CodigoOrdenCompra': 'Codigo de la Orden de Compra',
             'CodigoProveedor': 'Codigo del Proveedor',
             'Fecha': 'Fecha de Emision',
         }

         widgets = {
             'CodigoOrdenCompra': forms.TextInput(attrs={'class':'form-control'}),
             'CodigoProveedor': forms.TextInput(attrs={'class':'form-control'}),
             'Fecha': forms.TextInput(attrs={'class':'form-control'}),
         }


